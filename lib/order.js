Order = new Meteor.Collection( 'order' );

OrderSchema = new SimpleSchema({
  "restaurant_id": {
    type: String,
    denyUpdate: true
  },
  "created": {
    type: Number,
    denyUpdate: true,
    autoValue: function() {
      if ( this.isInsert ) {
        return new Date().getTime()
      }
    }
  },
  "total": {
    type: Number,
    decimal: true,
    denyUpdate: true
  },
  "customer": {
    type: Object
  },
  "customer.name": {
    type: String,
    denyUpdate: true
  },
  "customer.phone": {
    type: String,
    denyUpdate: true
  },
  "customer.email": {
    type: String,
    denyUpdate: true
  },
  "config": {
    type: Object
  },
  "config.payment": {
    type: Object
  },
  "config.payment.method": {
    type: String
  },
  "config.payment.captured": {
    type: Boolean,
    optional: true
  },
  "config.logistic": {
    type: String,
    denyUpdate: true
  },
  "config.location": {
    type: String,
    denyUpdate: true
  },
  "config.destination": {
    type: String,
    optional: true,
    denyUpdate: true
  },
  "config.distance": {
    type: Number,
    decimal: true,
    optional: true,
    denyUpdate: true
  },
  "config.driving_time": {
    type: Number,
    decimal: true,
    optional: true,
    denyUpdate: true
  },
  "config.when": {
    type: String,
    denyUpdate: true
  },
  "config.date": {
    type: String,
    optional: true,
    denyUpdate: true
  },
  "config.time": {
    type: String,
    optional: true,
    denyUpdate: true
  },
  "status": {
    type: String,
    autoValue: function() {
      if ( this.isInsert ) {
        return "Un-Confirmed"
      }
    }
  },
  // DISHES
  "dishes": {
    type: [Object],
    denyUpdate: true
  },
  "dishes.$._id": {
    type: String
  },
  "dishes.$.name": {
    type: String
  },
  "dishes.$.price": {
    type: Number,
    decimal: true
  },
  "dishes.$.description": {
    type: String
  },
  "dishes.$.category": {
    type: String
  },
  "dishes.$.quantity": {
    type: Number
  },
  "dishes.$.option_sets": {
    type: [Object],
  },
  "dishes.$.option_sets.$._id": {
    type: String
  },
  "dishes.$.option_sets.$.name": {
    type: String
  },
  "dishes.$.option_sets.$.multi_select": {
    type: Boolean
  },
  "dishes.$.option_sets.$.required": {
    type: Boolean
  },
  "dishes.$.option_sets.$.options": {
    type: [Object]
  },
  "dishes.$.option_sets.$.options.$._id": {
    type: String
  },
  "dishes.$.option_sets.$.options.$.name": {
    type: String
  },
  "dishes.$.option_sets.$.options.$.modifier": {
    type: Number,
    decimal: true,
    optional: true
  },
  "dishes.$.option_sets.$.options.$.isSet": {
    type: Boolean,
    optional: true
  },
});

Order.attachSchema( OrderSchema );
