const currencyList = [
  {
    _id: "ALL"
    ,name: "Albania Lek"
    ,symbol: "Lek"
  },
  {
    _id: "AFN"
    ,name: "Afghanistan Afghani"
    ,symbol: "؋"
  },
  {
    _id: "ARS"
    ,name: "Argentina Peso"
    ,symbol: "$"
  },
  {
    _id: "AWG"
    ,name: "Aruba Guilder"
    ,symbol: "ƒ"
  },
  {
    _id: "AUD",
    name: "Australia Dollar",
    symbol: "$"
  },
  {
    _id: "AZN"
    ,name: "Azerbaijan New Manat"
    ,symbol: "???"
  },
  {
    _id: "BSD"
    ,name: "Bahamas Dollar"
    ,symbol: "$"
  },
  {
    _id: "BBD"
    ,name: "Barbados Dollar"
    ,symbol: "$"
  },
  {
    _id: "BYR"
    ,name: "Belarus Ruble"
    ,symbol: "p."
  },
  {
    _id: "BZD"
    ,name: "Belize Dollar"
    ,symbol: "BZ$"
  },
  {
    _id: "BMD"
    ,name: "Bermuda Dollar"
    ,symbol: "$"
  },
  {
    _id: "BOB"
    ,name: "Bolivia Bolíviano"
    ,symbol: "$b"
  },
  {
    _id: "BAM"
    ,name: "Bosnia and Herzegovina Convertible Marka"
    ,symbol: "KM"
  },
  {
    _id: "BWP"
    ,name: "Botswana Pula"
    ,symbol: "P"
  },
  {
    _id: "BGN"
    ,name: "Bulgaria Lev"
    ,symbol: "лв"
  },
  {
    _id: "BRL"
    ,name: "Brazil Real"
    ,symbol: "R$"
  },
  {
    _id: "BND"
    ,name: "Brunei Darussalam Dollar"
    ,symbol: "$"
  },
  {
    _id: "KHR"
    ,name: "Cambodia Riel"
    ,symbol: "៛"
  },
  {
    _id: "CAD"
    ,name: "Canada Dollar"
    ,symbol: "$"
  },
  {
    _id: "KYD"
    ,name: "Cayman Islands Dollar"
    ,symbol: "$"
  },
  {
    _id: "CLP"
    ,name: "Chile Peso"
    ,symbol: "$"
  },
  {
    _id: "CNY"
    ,name: "China Yuan Renminbi"
    ,symbol: "¥"
  },
  {
    _id: "COP"
    ,name: "Colombia Peso"
    ,symbol: "$"
  },
  {
    _id: "CRC"
    ,name: "Costa Rica Colon"
    ,symbol: "₡"
  },
  {
    _id: "HRK"
    ,name: "Croatia Kuna"
    ,symbol: "kn"
  },
  {
    _id: "CUP"
    ,name: "Cuba Peso"
    ,symbol: "$MN"
  },
  {
    _id: "CZK"
    ,name: "Czech Republic Koruna"
    ,symbol: "Kč"
  },
  {
    _id: "DKK"
    ,name: "Denmark Krone"
    ,symbol: "kr"
  },
  {
    _id: "DOP"
    ,name: "Dominican Republic Peso"
    ,symbol: "RD$"
  },
  {
    _id: "XCD"
    ,name: "East Caribbean Dollar"
    ,symbol: "$"
  },
  {
    _id: "EGP"
    ,name: "Egypt Pound"
    ,symbol: "£"
  },
  {
    _id: "SVC"
    ,name: "El Salvador Colon"
    ,symbol: "$"
  },
  {
    _id: "EUR"
    ,name: "Euro Member Countries"
    ,symbol: "€"
  },
  {
    _id: "FKP"
    ,name: "Falkland Islands (Malvinas) Pound"
    ,symbol: "£"
  },
  {
    _id: "FJD"
    ,name: "Fiji Dollar"
    ,symbol: "$"
  },
  {
    _id: "GHS"
    ,name: "Ghana Cedi"
    ,symbol: "¢"
  },
  {
    _id: "GIP"
    ,name: "Gibraltar Pound"
    ,symbol: "£"
  },
  {
    _id: "GTQ"
    ,name: "Guatemala Quetzal"
    ,symbol: "Q"
  },
  {
    _id: "GGP"
    ,name: "Guernsey Pound"
    ,symbol: "£"
  },
  {
    _id: "GYD"
    ,name: "Guyana Dollar"
    ,symbol: "$"
  },
  {
    _id: "HNL"
    ,name: "Honduras Lempira"
    ,symbol: "L"
  },
  {
    _id: "HKD"
    ,name: "Hong Kong Dollar"
    ,symbol: "$"
  },
  {
    _id: "HUF"
    ,name: "Hungary Forint"
    ,symbol: "Ft"
  },
  {
    _id: "ISK"
    ,name: "Iceland Krona"
    ,symbol: "kr"
  },
  {
    _id: "INR"
    ,name: "India Rupee"
    ,symbol: "₹"
  },
  {
    _id: "IDR"
    ,name: "Indonesia Rupiah"
    ,symbol: "Rp"
  },
  {
    _id: "IRR"
    ,name: "Iran Rial"
    ,symbol: "?"
  },
  {
    _id: "IMP"
    ,name: "Isle of Man Pound"
    ,symbol: "£"
  },
  {
    _id: "ILS"
    ,name: "Israel Shekel"
    ,symbol: "₪"
  },
  {
    _id: "JMD"
    ,name: "Jamaica Dollar"
    ,symbol: "J$"
  },
  {
    _id: "JPY"
    ,name: "Japan Yen"
    ,symbol: "¥"
  },
  {
    _id: "JEP"
    ,name: "Jersey Pound"
    ,symbol: "£"
  },
  {
    _id: "KZT"
    ,name: "Kazakhstan Tenge"
    ,symbol: "??"
  },
  {
    _id: "KPW"
    ,name: "Korea (North) Won"
    ,symbol: "₩"
  },
  {
    _id: "KRW"
    ,name: "Korea (South) Won"
    ,symbol: "₩"
  },
  {
    _id: "KGS"
    ,name: "Kyrgyzstan Som"
    ,symbol: "сом"
  },
  {
    _id: "LAK"
    ,name: "Laos Kip"
    ,symbol: "₭"
  },
  {
    _id: "LBP"
    ,name: "Lebanon Pound"
    ,symbol: "£"
  },
  {
    _id: "LRD"
    ,name: "Liberia Dollar"
    ,symbol: "$"
  },
  {
    _id: "MKD"
    ,name: "Macedonia Denar"
    ,symbol: "ден"
  },
  {
    _id: "MYR"
    ,name: "Malaysia Ringgit"
    ,symbol: "RM"
  },
  {
    _id: "MUR"
    ,name: "Mauritius Rupee"
    ,symbol: "₨"
  },
  {
    _id: "MXN"
    ,name: "Mexico Peso"
    ,symbol: "$"
  },
  {
    _id: "MNT"
    ,name: "Mongolia Tughrik"
    ,symbol: "₮"
  },
  {
    _id: "MZN"
    ,name: "Mozambique Metical"
    ,symbol: "MT"
  },
  {
    _id: "NAD"
    ,name: "Namibia Dollar"
    ,symbol: "$"
  },
  {
    _id: "NPR"
    ,name: "Nepal Rupee"
    ,symbol: "₨"
  },
  {
    _id: "ANG"
    ,name: "Netherlands Antilles Guilder"
    ,symbol: "ƒ"
  },
  {
    _id: "NZD"
    ,name: "New Zealand Dollar"
    ,symbol: "$"
  },
  {
    _id: "NIO"
    ,name: "Nicaragua Cordoba"
    ,symbol: "C$"
  },
  {
    _id: "NGN"
    ,name: "Nigeria Naira"
    ,symbol: "₦"
  },
  {
    _id: "NOK"
    ,name: "Norway Krone"
    ,symbol: "kr"
  },
  {
    _id: "OMR"
    ,name: "Oman Rial"
    ,symbol: "?"
  },
  {
    _id: "PKR"
    ,name: "Pakistan Rupee"
    ,symbol: "Rs"
  },
  {
    _id: "PAB"
    ,name: "Panama Balboa"
    ,symbol: "B/."
  },
  {
    _id: "PYG"
    ,name: "Paraguay Guarani"
    ,symbol: "Gs"
  },
  {
    _id: "PEN"
    ,name: "Peru Sol"
    ,symbol: "S/."
  },
  {
    _id: "PHP"
    ,name: "Philippines Peso"
    ,symbol: "₱"
  },
  {
    _id: "PLN"
    ,name: "Poland Zloty"
    ,symbol: "?"
  },
  {
    _id: "QAR"
    ,name: "Qatar Riyal"
    ,symbol: "?"
  },
  {
    _id: "RON"
    ,name: "Romania New Leu"
    ,symbol: "lei"
  },
  {
    _id: "RUB"
    ,name: "Russia Ruble"
    ,symbol: "?"
  },
  {
    _id: "SHP"
    ,name: "Saint Helena Pound"
    ,symbol: "£"
  },
  {
    _id: "SAR"
    ,name: "Saudi Arabia Riyal"
    ,symbol: "?"
  },
  {
    _id: "RSD"
    ,name: "Serbia Dinar"
    ,symbol: "дин"
  },
  {
    _id: "SCR"
    ,name: "Seychelles Rupee"
    ,symbol: "?"
  },
  {
    _id: "SGD"
    ,name: "Singapore Dollar"
    ,symbol: "$"
  },
  {
    _id: "SBD"
    ,name: "Solomon Islands Dollar"
    ,symbol: "$"
  },
  {
    _id: "SOS"
    ,name: "Somalia Shilling"
    ,symbol: "S"
  },
  {
    _id: "ZAR"
    ,name: "South Africa Rand"
    ,symbol: "R"
  },
  {
    _id: "LKR"
    ,name: "Sri Lanka Rupee"
    ,symbol: "Rs"
  },
  {
    _id: "SEK"
    ,name: "Sweden Krona"
    ,symbol: "kr"
  },
  {
    _id: "CHF"
    ,name: "Switzerland Franc"
    ,symbol: "CHF"
  },
  {
    _id: "SRD"
    ,name: "Suriname Dollar"
    ,symbol: "$"
  },
  {
    _id: "SYP"
    ,name: "Syria Pound"
    ,symbol: "£"
  },
  {
    _id: "TWD"
    ,name: "Taiwan New Dollar"
    ,symbol: "NT$"
  },
  {
    _id: "THB"
    ,name: "Thailand Baht"
    ,symbol: "฿"
  },
  {
    _id: "TTD"
    ,name: "Trinidad and Tobago Dollar"
    ,symbol: "TT$"
  },
  {
    _id: "TRY"
    ,name: "Turkey Lira"
    ,symbol: ""
  },
  {
    _id: "TVD"
    ,name: "Tuvalu Dollar"
    ,symbol: "$"
  },
  {
    _id: "UAH"
    ,name: "Ukraine Hryvnia"
    ,symbol: "₴"
  },
  {
    _id: "GBP"
    ,name: "United Kingdom Pound"
    ,symbol: "£"
  },
  {
    _id: "USD"
    ,name: "United States Dollar"
    ,symbol: "$"
  },
  {
    _id: "UYU"
    ,name: "Uruguay Peso"
    ,symbol: "$U"
  },
  {
    _id: "UZS"
    ,name: "Uzbekistan Som"
    ,symbol: "??"
  },
  {
    _id: "VEF"
    ,name: "Venezuela Bolivar"
    ,symbol: "Bs"
  },
  {
    _id: "VND"
    ,name: "Vietnam Dong"
    ,symbol: "₫"
  },
  {
    _id: "YER"
    ,name: "Yemen Rial"
    ,symbol: "?"
  },
  {
    _id: "ZWD"
    ,name: "Zimbabwe Dollar"
    ,symbol: "Z$"
  }
];

currencyList.sort((a, b) => {
  if (a.name < b.name) return -1;
  if (a.name > b.name) return 1;
  return 0;
});


function newLocationTimes() {
  return {
    _id: new Mongo.ObjectID()._str,
    days: [
      { _id: 'Monday', times: [1] },
      { _id: 'Tuesday', times: [1] },
      { _id: 'Wednesday', times: [1] },
      { _id: 'Thursday', times: [1] },
      { _id: 'Friday', times: [1] },
      { _id: 'Saturday', times: [1] },
      { _id: 'Sunday', times: [1] },
    ]
  };
}


const locationTimes = {
  _id: 1,
  days: [
    { _id: 'Monday', times: [1] },
    { _id: 'Tuesday', times: [1] },
    { _id: 'Wednesday', times: [1] },
    { _id: 'Thursday', times: [1] },
    { _id: 'Friday', times: [1] },
    { _id: 'Saturday', times: [1] },
    { _id: 'Sunday', times: [1] },
  ]
};


const emptyLocationTimes = {
  _id: 1,
  days: [
    { _id: 'Monday', times: [] },
    { _id: 'Tuesday', times: [] },
    { _id: 'Wednesday', times: [] },
    { _id: 'Thursday', times: [] },
    { _id: 'Friday', times: [] },
    { _id: 'Saturday', times: [] },
    { _id: 'Sunday', times: [] },
  ]
};

const openingTimesEmpty = {
  Monday: [],
  Tuesday: [],
  Wednesday: [],
  Thursday: [],
  Friday: [],
  Saturday: [],
  Sunday: []
};

export { currencyList, locationTimes, openingTimesEmpty, emptyLocationTimes, newLocationTimes };