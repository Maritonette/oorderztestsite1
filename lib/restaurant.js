/**
 * Created by theNiglet on 24/07/2016.
 */
Restaurant = new Meteor.Collection( 'restaurant' );

RestaurantSchema = new SimpleSchema({
  "name": {
    type: String,
    label: "Restaurant Name"
  },
  "created": {
    type: Number,
    denyUpdate: true,
    autoValue: function() {
      if ( this.isInsert ) {
        return new Date().getTime()
      }
    }
  },
  "billing": {
    type: Object,
    autoValue: function() {
      if ( this.isInsert ) {
        return {
          status: 1,
          disable: false,
          stripe_id: null,
          source: null,
          subscription_id: null,
          locations: 500,
          last_payment: null,
          cancel_at: null
        }
      }
    }
  },
  "billing.status": {
    type: Number,
    min: 1,
    max: 4
  },
  "billing.disable": {
    type: Boolean
  },
  "billing.stripe_id": {
    type: String,
    optional: true
  },
  "billing.subscription_id": {
    type: String,
    optional: true
  },
  "billing.source": {
    type: String,
    optional: true
  },
  "billing.last_payment": {
    type: Number,
    optional: true,
  },
  "billing.cancel_at": {
    type: Number,
    optional: true,
  },
  "billing.locations": {
    type: Number,
    min: 1
  },
  "settings": {
    type: Object
  },
  // STORE INFO SETTINGS
  "settings.info": {
    type: Object
  },
  "settings.info.store_email": {
    type: String,
    optional: true
  },
  "settings.info.terms": {
    type: String,
    optional: true
  },
  // LOGISTIC SETTINGS
  "settings.logistic": {
    type: Object
  },
  "settings.logistic.pickup": {
    type: Object
  },
  "settings.logistic.pickup.enabled": {
    type: Boolean
  },
  "settings.logistic.pickup.notes": {
    type: String,
    optional: true
  },
  "settings.logistic.delivery": {
    type: Object
  },
  "settings.logistic.delivery.enabled": {
    type: Boolean
  },
  "settings.logistic.delivery.notes": {
    type: String,
    optional: true
  },
  "settings.logistic.delivery.conditions": {
    type: Object,
    autoValue: function() {
      if ( this.isInsert ) {
        return { min_order: null, max_distance: null, max_driving_time: null }
      }
    }
  },
  "settings.logistic.delivery.conditions.min_order": {
    type: Number,
    decimal: true,
    optional: true
  },
  "settings.logistic.delivery.conditions.max_distance": {
    type: Number,
    decimal: true,
    optional: true
  },
  "settings.logistic.delivery.conditions.max_driving_time": {
    type: Number,
    decimal: true,
    optional: true
  },
  // PAYMENT SETTINGS
  "settings.payment": {
    type: Object
  },
  "settings.payment.cash": {
    type: Object
  },
  "settings.payment.cash.enabled": {
    type: Boolean
  },
  "settings.payment.efptos": {
    type: Object
  },
  "settings.payment.efptos.enabled": {
    type: Boolean
  },
  "settings.payment.credit_card": {
    type: Object,
    autoValue: function() {
      if ( this.isInsert ) {
        return {
          enabled: false
        }
      }
    }
  },
  "settings.payment.credit_card.enabled": {
    type: Boolean
  },
  "settings.payment.stripe": {
    type: Object,
    autoValue: function() {
      if ( this.isInsert ) {
        return {
          connected: false,
          charges_enabled: null,
          token_type: null,
          publishable_key: null,
          user_id: null,
          refresh_token: null,
          access_token: null,
          scope: null,
          livemode: null,
          default_currency: null,
          override_currency: null
        }
      }
    }
  },
  "settings.payment.stripe.connected": {
    type: Boolean
  },
  "settings.payment.stripe.charges_enabled": {
    type: Boolean,
    optional: true
  },
  "settings.payment.stripe.token_type": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.publishable_key": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.user_id": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.refresh_token": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.access_token": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.scope": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.livemode": {
    type: Boolean,
    optional: true
  },
  "settings.payment.stripe.default_currency": {
    type: String,
    optional: true
  },
  "settings.payment.stripe.override_currency": {
    type: String,
    optional: true
  },
  // Region settings
  "settings.region": {
    type: Object
  },
  "settings.region.currency": {
    type: String,
    min: 3,
    max: 3
  },
  "settings.region.currency_symbol": {
    type: String,
  },
  "settings.region.timezone": {
    type: String
  },
  // Location settings
  "settings.locations": {
    type: [Object]
  },
  "settings.locations.$._id": {
    type: String
  },
  "settings.locations.$.created": {
    type: Number,
    autoValue: function() {
      if ( this.isInsert ) {
        return new Date().getTime()
      }
    }
  },
  "settings.locations.$.map_data": {
    type: Object
  },
  "settings.locations.$.map_data.geometry": {
    type: Object
  },
  "settings.locations.$.map_data.geometry.lat": {
    type: Number,
    decimal: true,
    optional: true
  },
  "settings.locations.$.map_data.geometry.lng": {
    type: Number,
    decimal: true,
    optional: true
  },
  "settings.locations.$.address": {
    type: String
  },
  "settings.locations.$.open": {
    type: Boolean
  },
  "settings.locations.$.opening_times": {
    type: Object
  },
  "settings.locations.$.opening_times.Monday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Monday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Monday.$.close": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Tuesday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Tuesday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Tuesday.$.close": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Wednesday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Wednesday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Wednesday.$.close": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Thursday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Thursday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Thursday.$.close": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Friday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Friday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Friday.$.close": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Saturday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Saturday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Saturday.$.close": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Sunday": {
    type: [Object]
  },
  "settings.locations.$.opening_times.Sunday.$.open": {
    type: String,
    optional: true
  },
  "settings.locations.$.opening_times.Sunday.$.close": {
    type: String,
    optional: true
  },
  // Status settings
  "settings.status": {
    type: Object,
    autoValue: function() {
      if ( this.isInsert ) {
        return { auto: {
          enabled: false,
          time: {
            confirmed: null,
            ready: null,
            on_route: null,
            complete: null
            }
          }
        }
      }
    }
  },
  "settings.status.auto": {
    type: Object
  },
  "settings.status.auto.enabled": {
    type: Boolean
  },
  "settings.status.auto.time": {
    type: Object
  },
  "settings.status.auto.time.confirmed": {
    type: Number,
    optional: true
  },
  "settings.status.auto.time.ready": {
    type: Number,
    optional: true
  },
  "settings.status.auto.time.on_route": {
    type: Number,
    optional: true
  },
  "settings.status.auto.time.complete": {
    type: Number,
    optional: true
  },
  // DESIGN SETTINGS
  "settings.design": {
    type: Object,
    autoValue: function() {
      if ( this.isInsert ) {
        return {
          colors: {
            primary : "rgba(248,153,75,1)",
            primary_text : "rgba(255,255,255,1)",
            main_text : "rgba(19,19,19,1)",
            menu_text : "rgba(167,177,194,1)",
            background : "rgba(19,19,19,1)",
            box : "rgba(243,243,244,1)",
            secondary_box : "rgba(255,255,255,1)",
            menu : "rgba(25,25,25,1)",
            border: "rgba(255,255,255,1)",
            input: "rgba(248,153,75,1)",
            input_text: "rgba(19,19,19,1)"
          },
          images: {
            logo: { url: null, rel: null },
            background: { url: null, rel: null, stretch: false }
          }
        }
      }
    }
  },
  "settings.design.colors": {
    type: Object,
  },
  "settings.design.colors.primary": {
    type: String,
    max: 22,
  },
  "settings.design.colors.primary_text": {
    type: String,
    max: 22,
  },
  "settings.design.colors.main_text": {
    type: String,
    max: 22,
  },
  "settings.design.colors.menu_text": {
    type: String,
    max: 22,
  },
  "settings.design.colors.background": {
    type: String,
    max: 22,
  },
  "settings.design.colors.menu": {
    type: String,
    max: 22,
  },
  "settings.design.colors.box": {
    type: String,
    max: 22,
  },
  "settings.design.colors.secondary_box": {
    type: String,
    max: 22,
  },
  "settings.design.colors.border": {
    type: String,
    max: 22,
  },
  "settings.design.colors.input": {
    type: String,
    max: 22,
  },
  "settings.design.colors.input_text": {
    type: String,
    max: 22,
  },
  "settings.design.images": {
    type: Object,
  },
  "settings.design.images.logo": {
    type: Object,
  },
  "settings.design.images.logo.url": {
    type: String,
    optional: true
  },
  "settings.design.images.logo.rel": {
    type: String,
    optional: true
  },
  "settings.design.images.background": {
    type: Object,
  },
  "settings.design.images.background.url": {
    type: String,
    optional: true
  },
  "settings.design.images.background.rel": {
    type: String,
    optional: true
  },
  "settings.design.images.background.stretch": {
    type: Boolean
  },
  // DISHES
  "dishes": {
    type: [Object],
    autoValue: function() {
      if ( this.isInsert ) {
        return []
      }
    }
  },
  "dishes.$._id": {
    type: String
  },
  "dishes.$.name": {
    type: String
  },
  "dishes.$.price": {
    type: Number,
    decimal: true
  },
  "dishes.$.description": {
    type: String
  },
  "dishes.$.category": {
    type: String
  },
  "dishes.$.image": {
    type: Object
  },
  "dishes.$.image.url": {
    type: String,
    optional: true
  },
  "dishes.$.image.rel": {
    type: String,
    optional: true
  },
  "dishes.$.option_sets": {
    type: [Object],
  },
  "dishes.$.option_sets.$._id": {
    type: String
  },
  "dishes.$.option_sets.$.name": {
    type: String
  },
  "dishes.$.option_sets.$.multi_select": {
    type: Boolean
  },
  "dishes.$.option_sets.$.required": {
    type: Boolean
  },
  "dishes.$.option_sets.$.options": {
    type: [Object]
  },
  "dishes.$.option_sets.$.options.$._id": {
    type: String
  },
  "dishes.$.option_sets.$.options.$.name": {
    type: String
  },
  "dishes.$.option_sets.$.options.$.modifier": {
    type: Number,
    decimal: true,
    optional: true
  },
  "dishes.$.option_sets.$.options.$.isSet": {
    type: Boolean,
    optional: true
  },
  // option sets, excludes, promos
  // CATEGORIES
  "categories": {
    type: [Object],
    autoValue: function() {
      if ( this.isInsert ) {
        return []
      }
    }
  },
  "categories.$._id": {
    type: String
  },
  "categories.$.name": {
    type: String
  },
  // OPTION SETS
  "option_sets": {
    type: [Object],
    autoValue: function() {
      if ( this.isInsert ) {
        return []
      }
    }
  },
  "option_sets.$._id": {
    type: String
  },
  "option_sets.$.name": {
    type: String
  },
  "option_sets.$.multi_select": {
    type: Boolean
  },
  "option_sets.$.required": {
    type: Boolean
  },
  "option_sets.$.options": {
    type: [Object]
  },
  "option_sets.$.options.$._id": {
    type: String
  },
  "option_sets.$.options.$.name": {
    type: String
  },
  "option_sets.$.options.$.modifier": {
    type: Number,
    decimal: true,
    optional: true
  },
  "option_sets.$.options.$.isSet": {
    type: Boolean,
    optional: true
  },
  // ORDERS
  "orders": {
    type: [String],
    autoValue: function() {
      if ( this.isInsert ) {
        return []
      }
    }
  }
});

Restaurant.attachSchema( RestaurantSchema );
