// LAYOUT CONFIGURATIONS
Router.configure({
  layoutTemplate: 'mainLayout',
  notFoundTemplate: 'notFound',
  loadingTemplate: 'loading'
});

Router.onAfterAction(function() {
  Meteor.setTimeout(() =>{
    $(window).resize();
  }, 10);
});


Router.onBeforeAction(function() {
  if (!Meteor.userId() && this.ready()) {
    $.unblockUI();
    this.redirect('login');
  }
  else {
    this.next();
  }
}, {only: ['emailVerification', 'dashboard', 'restaurantSetup', 'restaurantSettings', 'restaurantBilling', 'restaurantDishes', 'restaurantCategories', 'restaurantOptionSets', 'restaurantViewOrders']});

Router.onBeforeAction(function() {
  const user = Meteor.user();
  if (user && !user.emails[0].verified) {
    this.redirect('emailVerification');
  }
  this.next();
}, {only: ['dashboard', 'restaurantSetup', 'restaurantSettings', 'restaurantBilling', 'restaurantDishes', 'restaurantCategories', 'restaurantOptionSets', 'restaurantViewOrders', 'integrationStripe']});

/*
Router.onBeforeAction(function() {
  //const r = Restaurant.find({ user_id: Meteor.userId() });
  const user = Meteor.user();
  if (user && !user.account.restaurant_id) {
    this.redirect('restaurantSetup');
  }
  this.next();
}, {only: ['dashboard']});
*/

// FRONT ROUTES

Router.route('/', {
  path: '/',
  name: 'home',
  template: 'landing',
  layoutTemplate: 'blankLayout',
});

// AUTH ROUTES

Router.route('/login', {
  path: '/login',
  name: 'login',
  template: 'login',
  layoutTemplate: 'blankLayout',
  onAfterAction: function() {
    if (Meteor.userId())
      return this.redirect('/dashboard');
  }
});

Router.route('/register', {
  path: '/register',
  name: 'register',
  template: 'register',
  layoutTemplate: 'blankLayout',
  onBeforeAction: function() {
    if (Meteor.userId())
      return this.redirect('/dashboard');
    else
      this.next();
  }
});

Router.route('/forgotten-password', {
  path: '/forgotten-password',
  name: 'forgotPassword',
  template: 'forgotPassword',
  layoutTemplate: 'blankLayout',
});

Router.route('/reset-password/:id', {
  path: '/reset-password/:id',
  name: 'resetPassword',
  template: 'resetPassword',
  layoutTemplate: 'blankLayout',
});

Router.route('/account/email-verification', {
  path: '/account/email-verification',
  name: 'emailVerification',
  template: 'emailVerification',
  layoutTemplate: 'blankLayout',
});

// EMAIL VERIFICATION

Router.route('/verify-email/:_id', {
  // this template will be rendered until the subscriptions are ready
  //loadingTemplate: 'loading',
  path: '/verify-email/:_id',
  name: 'verifyEmail',
  layoutTemplate: 'blankLayout',
  action: function () {
    $.blockUI({message : '<p style="font-size: 2rem; color: #c3c3c3; margin-bottom: 2rem;">Verifying Email...</p>' +
    '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    Accounts.verifyEmail( this.params._id, ( error ) => {
      if ( error ) {
        Router.go('/account/email-verification');
        toastr.error('Error', error.reason);
      } else {
        Router.go('/dashboard/restaurant-setup');
        toastr.success('Email verified! Thanks!');
      }
      $.unblockUI();
    });
  }
});

// DASHBOARD ROUTES

Router.route('/dashboard', {
  path: '/dashboard',
  name: 'dashboard',
  template: 'dashboard',
  loadingTemplate: 'loading'
});

Router.route('/dashboard/restaurant-setup', {
  path: '/dashboard/restaurant-setup',
  name: 'restaurantSetup',
  template: 'restaurantSetup'
});

Router.route('/dashboard/settings', {
  path: '/dashboard/settings',
  name: 'restaurantSettings',
  template: 'restaurantSettings'
});

Router.route('/dashboard/billing', {
  path: '/dashboard/billing',
  name: 'restaurantBilling',
  template: 'restaurantBilling'
});


// MENU ROUTES

Router.route('/dashboard/menu/dishes', {
  path: '/dashboard/menu/dishes',
  name: 'restaurantDishes',
  template: 'restaurantDishes'
});

Router.route('/dashboard/menu/categories', {
  path: '/dashboard/menu/categories',
  name: 'restaurantCategories',
  template: 'restaurantCategories'
});

Router.route('/dashboard/menu/option-sets', {
  path: '/dashboard/menu/option-sets',
  name: 'restaurantOptionSets',
  template: 'restaurantOptionSets'
});

// ORDERS

Router.route('/dashboard/orders', {
  path: '/dashboard/orders',
  name: 'restaurantViewOrders',
  template: 'restaurantViewOrders'
});

// STORE ROUTES

Router.route('/restaurant/:id', {
  path: '/restaurant/:id',
  name: 'storeStart',
  template: 'storeStart',
  layoutTemplate: 'storeLayout'
});

Router.route('/restaurant/store/:id', {
  path: '/restaurant/store/:id',
  name: 'storeMain',
  template: 'storeMain',
  layoutTemplate: 'storeLayout'
});

Router.route('/order/:oid/:id', {
  path: '/order/:oid/:id',
  name: 'orderConfirmation',
  template: 'orderConfirmation',
  layoutTemplate: 'storeLayout'
});

Router.onBeforeAction(function() {
  $.blockUI({
    message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>',
    overlayCSS: {
      opacity: 1
    },
    fadeIn:  0
  });
  this.next();
}, {only: ['storeStart', 'storeMain']});


// INTERGRATION ROUTES

// STRIPE
Router.route('/integrations/stripe', {
  // this template will be rendered until the subscriptions are ready
  //loadingTemplate: 'loading',
  path: '/integrations/stripe',
  name: 'integrationStripe',
  layoutTemplate: 'blankLayout',
  action() {
    $.blockUI({message : '<p style="font-size: 2rem; color: #c3c3c3; margin-bottom: 2rem;">Connecting Stripe...</p>' +
    '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});

    let query = this.params.query;
    let code = query.code;
    if (code) {
      Meteor.call('connectStripe', code, (err, res) => {
        if (err) {
          $.blockUI({message : '<p style="font-size: 2rem; color: #c3c3c3; margin-bottom: 1.5rem;">Something went wrong, please try again or contact  (redirecting ..)</p>' +
          '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
          Meteor.setTimeout(() => {
            Router.go('/dashboard/settings');
            $.unblockUI();
          }, 5000)
        }
        else {
          $.blockUI({message : '<p style="font-size: 2rem; color: #c3c3c3; margin-bottom: 1.5rem;">Stripe Connected (redirecting...)</p>' +
          '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
          Meteor.setTimeout(() => {
            Router.go('/dashboard/settings');
            $.unblockUI();
          }, 2000)
        }
      });
    }
    else {
      Router.go('/dashboard/settings');
      $.unblockUI();
    }

  }
});

