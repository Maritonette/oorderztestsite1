Meteor.startup(() => {

  let rxValidRgb = /([R][G][B][A]?[(]\s*([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\s*,\s*([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\s*,\s*([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])(\s*,\s*((0\.[0-9]{1})|(1\.0)|(1)))?[)])/i;
  $.validator.addMethod("rgba", function(value, element) {
    return this.optional(element) || rxValidRgb.test(value);
  }, "Enter a RGBA value or use to color picker");

  GoogleMaps.load({ v: '3', key: 'AIzaSyDSz1Rkjk015pmSchnf27a9Wm0hRidbsbg', libraries: 'geometry,places' });

  Meteor.users.deny({
    update() { return true; }
  });

  toastr.options = {
    "positionClass": "toast-bottom-left",
  }

});