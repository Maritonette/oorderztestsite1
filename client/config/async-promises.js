/**
 * Created by theNiglet on 14/04/2016.
 */

export function S3UploadePromise(files, maxSize, path) {
  var promise = new Promise((resolve,reject) => {
    if(files.length == 0) resolve('No File');
    if(files[0].size > maxSize) reject("Image is too large");
    else{
      S3.upload({ files: files, path: path, unqiue_name: true }, (e, r) => {
        if(e) reject(e);
        else resolve(r)
      });
    }
  });
  return promise;
}

export function S3DeletePromise(url) {
  var promise = new Promise((resolve,reject) => {
    if (!url) resolve(null);
    else {
      S3.delete(url, (e, r) => {
        if(e) reject(e);
        else resolve(r)
      });
    }
  });
  return promise;
}

// GOOGLE MAPS API

export function DistanceMatrix(origins, destinations) {
  var promise = new Promise((resolve,reject) => {
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
      origins: origins,
      destinations: destinations,
      travelMode: google.maps.TravelMode.DRIVING,
    }, (res, status) => {
      if (status == google.maps.DistanceMatrixStatus.OK) resolve(res);
      else reject(status);
    });
  });
  return promise;
}

export function GoogleMapsPlaceGetCords(address) {
  var promise = new Promise((resolve,reject) => {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
      'address': address
    }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          resolve(results);
          //map.setCenter(results[0].geometry.location);
        } else {
          reject("No results found");
        }
      } else {
        reject("Geocode was not successful for the following reason: " + status);
      }
    });
  });
  return promise;
}
