Template.register.onRendered(function (){

});

Template.register.events({

  'submit #register-form'(e, t) {
    e.preventDefault();
    const p1 = $('#register-password').val();
    const p2 = $('#confirm-password').val();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    if (!$('#register-accept').prop('checked')) {
      toastr.error('Please accept the terms & conditions before proceeding');
      $.unblockUI();
    }
    else if (p1 !== p2) {
      toastr.error('Passwords do not match');
      $.unblockUI();
    }
    else {
      const email = ($('#register-email').val()).replace(/^\s*|\s*$/g, ""); // Trim whitespace
      Accounts.createUser({email: email, password : p1}, function(err){
        $.unblockUI();
        if (err) toastr.warning('Error', err);
        else {
          Meteor.call('sendVerificationLink');
          Router.go('/dashboard');
        }
      });
    }
  }

});