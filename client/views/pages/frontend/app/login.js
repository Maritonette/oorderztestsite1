Template.login.onRendered(function (){

});

Template.login.events({
  'submit #login-form'(e, t) {
    e.preventDefault();
    const email = ($('#login-email').val()).replace(/^\s*|\s*$/g, "") // Trim whitespace
      , password = $('#login-password').val();

    Meteor.loginWithPassword(email, password, function(err){
      if (err) toastr.warning('Login Failed', err);
      else Router.go('/dashboard');
    });
  }
});