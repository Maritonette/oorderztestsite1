Template.emailVerification.events({
  'click .resend-verification-link' ( event, template ) {
    Meteor.call( 'sendVerificationLink', ( error, response ) => {
      if ( error ) {
        toastr.error('Error', error.reason);
      } else {
        let email = Meteor.user().emails[0].address;
        toastr.success( `Verification sent to ${ email }!`);
      }
    });
  }
});

Template.emailVerification.onCreated(function () {

  const userHandle = this.subscribe('user.private');

  Tracker.autorun(() => {
    const userReady = userHandle.ready();
    if (userReady) {
      const u = Meteor.user();
      if (u.emails[0].verified) {
        Router.go('/dashboard')
      }
    }
  });

});