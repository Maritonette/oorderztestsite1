Template.forgotPassword.onRendered(function (){

});

Template.forgotPassword.events({

  'submit #forgotPasswordForm'(e, t) {
    e.preventDefault();
    const email = $('#forgotPwdEmail').val();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    Accounts.forgotPassword({email: email}, function(err){
      $.unblockUI();
      if (err) toastr.warning('Error', err);
      else {
        toastr.success('Password reset email sent')
      }
    });
  }

});