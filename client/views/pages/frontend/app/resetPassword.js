Template.resetPassword.onRendered(function (){

});

Template.resetPassword.onCreated(function (){
  const token = Router.current().params.id;
  Session.set('reset.token', token);
  if (!token) {
    Router.go('/')
  }
});

Template.resetPassword.events({
  'submit #resetPasswordForm'(e, t) {
    e.preventDefault();
    const p1 = $('#reset-password').val();
    const p2 = $('#reset-password2').val();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    if (p1 !== p2) {
      toastr.error('Passwords do not match');
      $.unblockUI();
    }
    else {
      Accounts.resetPassword(Session.get('reset.token'), p1, function(err){
        $.unblockUI();
        if (err) toastr.warning('Error', err);
        else {
          toastr.success('Password Reset');
          Router.go('/login')
        }
      });
    }
  },
});