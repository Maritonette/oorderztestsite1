Template.orderConfirmation.onRendered(function OnRendered() {



});

Template.orderConfirmation.onDestroyed(function OnRendered() {


});

Template.orderConfirmation.onCreated(function OnCreated() {

  // RESET ALL ORDER VARIABLES SO THEY CANT GO BACK
  Session.set('order.setup', null);
  Session.set('os_logistic', null);
  Session.set('os_location', null);
  Session.set('os_when', null);
  Session.set('os_date', null);
  Session.set('os_time', null);
  Session.set('os_destination', null);
  Session.set('os_distance', null);

  let self = this;

  const oid = Router.current().params.oid;
  const handle = this.subscribe('order.public', oid);
  Tracker.autorun(() => {
    const ready = handle.ready();
    if (ready) {
      
      const o = Order.findOne();
      Session.set('order.public', o);
      Session.set('restaurant.id', o.restaurant_id);
      self.subscribe('restaurant.public', o.restaurant_id)


    }
  });

  Meteor.autorun(function () {
    let stat;
    if (Meteor.status().status === "connected") stat = 'green';
    else if (Meteor.status().status === "connecting") stat = 'yellow';
    else stat = 'red';
    Session.set('status',stat);
  });

});

Template.orderConfirmation.helpers({
  status() {
    return Session.get('status')
  },
  r() {
    return Restaurant.findOne();
  },
  cIcon() {
    const r = Restaurant.findOne();
    if (r) {
      return r.settings.region.currency_symbol;
    }
  },
  o() {
    return Session.get('order.public');
  },
  addOne(a) {
    return a + 1
  },
  equals(a, b) {
    return a === b;
  },
  location() {
    let order = Session.get('order.public');
    if (order && order.config) {
      return findLocation(order.config.location);
    }
  },
  orderTime() {
    let order = Session.get('order.public');
    const r = Restaurant.findOne();
    if (order && order.config && r && r.settings) {
      if (order.config.when == 'Later') {
        return `Later - ${order.config.date} ${order.config.time} (dd/mm/yy)`
      }
      if (order.config.when == 'Now') {
        let now = moment.utc(order.created).tz(r.settings.region.timezone);
        let time = now.format('h:m A');
        let date = now.format('dddd');
        return `Now - ${time} ${date}`
      }
    }
  }
});

Template.orderConfirmation.events({

});

function findLocation(id) {
  let index = null;
  const r = Restaurant.findOne();
  if (r) {
    r.settings.locations.forEach((p, i) => { if (p._id == id ) index = i; });
    return r.settings.locations[index];
  }
}