import { DistanceMatrix } from '../../../../config/async-promises.js';

Template.storeStart.onRendered(function OnRendered() {



});

Template.storeStart.onDestroyed(function OnRendered() {


});

Template.storeStart.onCreated(function OnCreated() {

  this.viewData = new ReactiveDict();
  let template = Template.instance();

  Session.set('order.setup', null);

  Session.set('os_logistic', null);
  Session.set('os_location', null);
  Session.set('os_when', null);

  Session.set('os_date', null);
  Session.set('os_time', null);

  Session.set('os_destination', null);
  Session.set('os_distance', null);
  Session.set('os_driving_time', null);

  // NORMAL SUBSCRIPTION + STORE VALIDATION

  const rid = Router.current().params.id;

  const handle = this.subscribe('restaurant.public', rid);
  Tracker.autorun(() => {
    const ready = handle.ready();
    if (ready) {
      Session.clear('restaurant.public');
      const r = Restaurant.findOne();
      Session.set('restaurant.public', r);

      // DISABLE
      if (r.billing.disable) history.go(-1) ? history.go(-1) : window.location = 'http://www.google.com';
      else $.unblockUI();

      // AT LEAST ONE RESTAURANT OPEN
      let available = false;
      if (r.settings.locations.length > 0) {
        r.settings.locations.forEach((p, i) => { if (p.open) available = true; });
      }
      Session.set('oneAvailable', available);

      const logistics_enabled = [
        {
          name: "Pickup",
          enabled: r.settings.logistic.pickup.enabled,
        },
        {
          name: "Delivery",
          enabled: r.settings.logistic.delivery.enabled,
        }
      ];
      let nameOfAvailableLogistic = null;
      let numberOfLogisticsAvailable = 0;
      logistics_enabled.forEach((p, i) => {
        if (p.enabled) {
          numberOfLogisticsAvailable += 1;
          nameOfAvailableLogistic = p.name;
        }
      });
      if (numberOfLogisticsAvailable == 1) Session.set('os_logistic', nameOfAvailableLogistic);
      template.viewData.set('numberOfLogisticsAvailable', numberOfLogisticsAvailable);
      template.viewData.set('nameOfLogisticAvailable', nameOfAvailableLogistic);
    }
  });

  // GOOGLE AUTOCOMPLETE TRACKER - INIT AUTOCOMPLETE - UPDATES RESTAURANT LOCATION ID
  // ADD DELIVERY CONDITIONS HERE
  Tracker.autorun(() => {
    const logistic = Session.get('os_logistic');
      if (logistic == 'Delivery' && GoogleMaps.loaded()) {
        Meteor.setTimeout(() => {
          const r = Session.get('restaurant.public');
          // INIT AUTOCOMPLETE
          let ac = new google.maps.places.Autocomplete(document.getElementById('orderDeliveryAddress'));
          // CLEAR PREVIOUS LISTNERS
          google.maps.event.clearListeners(ac, 'place_changed');
          // ADD NEW LISTENER TO CHECK IF ADDRESS FIELD CHANGES
          google.maps.event.addListener(ac, 'place_changed', function () {
            // GET THE PLACE ON CHANGE
            $('#deliveryAddressHelper').html(``);
            let place = ac.getPlace();
            if (!place.geometry) {
              $('#deliveryAddressHelper').html(`<p class="m-b-none text-danger animated fadeInUp">Delivery address not found or valid</p>`);
            }
            else {
              // GET CORDS OF THE DELIVERY LOCATION
              let destination = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
              // GET A LIST OF ALL THE RESTAURANT LOCATIONS
              const locations = [];
              r.settings.locations.forEach((p)=> { if (p.open) locations.push(p.address); });
              if (locations.length > 0) {

                // USE THE DISTANCE MATRIX TO GET THE DISTANCES FROM THE DELIVERY LOCATION
                const distanceMatrix = DistanceMatrix(locations, [destination]);
                distanceMatrix.then((res) => {
                  // FIND THE LOCATION CLOSEST TO THE DELIVERY ADDRESS
                  let distance = null;
                  let drivingTime = null;
                  let closestLocation = null;
                  if (!res.rows[0].elements[0].distance) {
                    $('#deliveryAddressHelper').html(`<p class="m-b-none text-danger animated fadeInUp">Delivery address not found or valid</p>`);
                  }
                  else {
                    res.rows.forEach((p, i) => {
                      if (!distance) {
                        distance = p.elements[0].distance.value;
                      }
                      else {
                        if (p.elements[0].distance.value < distance) {
                          distance = p.elements[0].distance.value;
                        }
                      }
                      if (!drivingTime) {
                        drivingTime = p.elements[0].duration.value;
                        closestLocation = locations[i];
                      }
                      else {
                        if (p.elements[0].duration.value < drivingTime) {
                          drivingTime = p.elements[0].duration.value;
                          closestLocation = locations[i];
                        }
                      }
                    });
                    // CLOSEST LOCATION FOUND
                    if (closestLocation) {
                      let maxDistance = r.settings.logistic.delivery.conditions.max_distance * 1000;
                      let maxDrivingTime = r.settings.logistic.delivery.conditions.max_driving_time * 60;
                      let deliveryConditionsError = false;
                      if (maxDistance) if (distance > maxDistance) deliveryConditionsError = true;
                      if (maxDrivingTime) if (drivingTime > maxDrivingTime) deliveryConditionsError = true;
                      if (deliveryConditionsError) {
                        toastr.error('Your address is not within our delivery range');
                        $('#deliveryAddressHelper').html(`<p class="m-b-none text-danger animated fadeInUp">Your address is not within our delivery range</p>`);
                      }
                      else {
                        let locationIndex = null;
                        r.settings.locations.forEach((p, i) => { if (p.address == closestLocation) locationIndex = i });
                        $('#deliveryAddressHelper').html(`<p class="m-b-none primary-color-text animated fadeInUp">Your store is ${closestLocation}</p>`);
                        Session.set('os_location',r.settings.locations[locationIndex]._id);
                        Session.set('os_destination', place.formatted_address);
                        Session.set('os_distance', distance); // meters
                        Session.set('os_driving_time', drivingTime); // seconds
                      }

                    }
                    else {
                      $('#deliveryAddressHelper').html(`<p class="m-b-none text-danger animated fadeInUp">Something went wrong, please re-select your address</p>`);
                    }
                  }
                }).catch((status) => {
                  $('#deliveryAddressHelper').html(`<p class="m-b-none text-danger animated fadeInUp">Something went wrong, please re-select your address</p>`);
                });

              }
              else {
                $('#deliveryAddressHelper').html(`<p class="m-b-none text-danger animated fadeInUp">All restaurant locations are closed</p>`);
              }

            }
          });
        }, 20);
      }

  });

  // VIEW TRACKERS
  Tracker.autorun(() => {
    const logistic = Session.get('os_logistic');
    Meteor.setTimeout(() => { $('#orderLocation').select2({ placeholder: 'Select Pickup Address' }); }, 10);
  });

  // VIEW TRACKERS
  Tracker.autorun(() => {
    const when = Session.get('os_when');
    Meteor.setTimeout(() => { $('#orderDate').select2({ placeholder: 'Select A Date', minimumResultsForSearch: -1}); }, 10);
  });

  // VIEW TRACKERS
  Tracker.autorun(() => {
    const date = Session.get('os_date');
    Meteor.setTimeout(() => {
      /*
      $('#orderTimeInputGroup').remove();
      $('#orderTime').before(`<span id="orderTimeInputGroup" class="input-group-addon" for="orderTime"><i class="fa fa-clock-o"></i></span>`);
      $('.ui-timepicker-select').select2({ minimumResultsForSearch: -1 });
      if ( $('.jq-timepicker .select2').length > 1 ) $('.jq-timepicker .select2:last-child').remove()
      */
    }, 50);
  });


  // TRACKER FOR TIME INPUT

  // TRACKER FOR TIME INPUT
  Tracker.autorun(() => {
    const os_when = Session.get('os_when');
    const os_location = Session.get('os_location');
    const os_date = Session.get('os_date');
    const r = Session.get('restaurant.public');
    if (r && r.settings.locations && os_location && os_when == 'Later' && os_date) {
      Meteor.setTimeout(() => {

        // GET CURRENT DAY AND CREATE DISABLED HOURS LIST
        let day = moment.utc(os_date, 'DD/MM/YYYY').tz(r.settings.region.timezone).startOf('day');
        const displayDay = day.format('dddd');
        let ind = findLocationIndex(os_location);
        const opening_times = r.settings.locations[ind].opening_times[displayDay];
        let disabledHours = [];
        disabledHours = [ day.format('h:mma') ];
        opening_times.forEach((p, i) => {
          const open = p.open.split(":");
          const close = p.close.split(":");
          disabledHours.push(day.set({ 'hour': open[0], 'minute': open[1] }).format('h:mma'));
          disabledHours.push(day.set({ 'hour': close[0], 'minute': close[1] }).format('h:mma'));
        });
        disabledHours.push(day.endOf('day').format('h:mma'));

        // INIT TIMEPICKER WITH DISABLED HOURS

        $('#orderTime').timepicker({
          orientation: 'tr',
          useSelect: true,
          className: 'form-control',
          step: 15,
          disableTimeRanges: partition(disabledHours, 2),
        }).on('change', function(){
          const time =  $('#orderTime').val();
          Session.set('os_time', time);
        });

        $('option').each(function() {
          if ($(this).attr('disabled')) $(this).remove();
        });

        // SET ORDER TIME TO FIRST AVAILABLE TIME SLOT WHEN DAY CHANGES

        let foundit = true;
        $('.ui-timepicker-select option').each(function() {
          if (!$(this).attr('disabled') && foundit) {
            foundit = false;
            $('#orderTime').timepicker('setTime', $(this).val());
            Session.set('os_time', $(this).val());
          }
        });

        $('#orderTimeInputGroup').remove();
        $('.ui-timepicker-select').select2({ minimumResultsForSearch: -1 }).addClass('animated fadeInUp');
        $('.jq-timepicker .select2').addClass('animated fadeInUp').css('width', '100%');
        $('#orderTime').before(`<span id="orderTimeInputGroup" class="input-group-addon animated fadeInUp" for="orderTime"><i class="fa fa-clock-o"></i></span>`);
        if ( $('.jq-timepicker .select2').length > 1 ) $('.jq-timepicker .select2:last-child').remove()

      }, 20);
    }
  });

});

Template.storeStart.helpers({
  r() {
    return Session.get('restaurant.public');
  },
  oneAvailable() {
    return Session.get('oneAvailable');
  },
  numberOfLogisticsAvailable() {
    return Template.instance().viewData.get('numberOfLogisticsAvailable');
  },
  nameOfLogisticAvailable() {
    return Template.instance().viewData.get('nameOfLogisticAvailable');
  },
  multi() {
    const r = Session.get('restaurant.public');
    if (r && r.settings.locations)
      return r.settings.locations.length > 1;
  },
  orderSetup() {
    return {
      logistic: Session.get('os_logistic'),
      location: Session.get('os_location'),
      when: Session.get('os_when'),
      date: Session.get('os_date')
    };
  },
  confirm() {
    const logistic = Session.get('os_logistic');
    const location = Session.get('os_location');
    const when = Session.get('os_when');
    const date = Session.get('os_date');
    const time = Session.get('os_time');
    const r = Session.get('restaurant.public');

    if (location && logistic) {
      if (when == 'Now') {
        return true;
      }
      else if (when == 'Later') {
        if (date && time) return true;
      }
    }
  },
  equals(a, b) {
    return a === b;
  },
  next10days() {
    const r = Session.get('restaurant.public');
    const location = Session.get('os_location');
    if (r && r.settings.locations && location) {

      // GET THE DAYS CLOSED
      let ind = findLocationIndex(location);
      const opening_times = r.settings.locations[ind].opening_times;
      const closedDays = [];
      for (var day in opening_times) {
        if (opening_times.hasOwnProperty(day)) {
          if (!opening_times[day][0].open && !opening_times[day][0].close) {
            closedDays.push(day);
          }
        }
      }

      const today = moment.utc(Date.now()).tz(r.settings.region.timezone);
      const next = [];

      do {
        if (!_.contains(closedDays, today.format('dddd'))) {
          next.push({
            _id: today.format('DD/MM/YYYY'),
            name: today.format('dddd - Do - MMM')
          });
        }
        today.add(1, 'days');
      }
      while (next.length < 7);

      return next;
    }
  },
  or(a, b) {
    if (a || b) return true
  }
});

Template.storeStart.events({
  'change .orderLogisticEvent'(e, ins) {
    const type = $(e.currentTarget).attr('data-id');
    // RESET IT ALL
    Session.set('os_location', null);
    Session.set('os_when', null);
    Session.set('os_date', null);
    Session.set('os_time', null);
    // SET LOGISTIC
    Session.set('os_logistic', type);
  },
  'change #orderLocation'(e, ins) {
    const location = $(e.currentTarget).val();
    Session.set('os_location', location);
  },
  'click .orderTimeEvent'(e, ins) {
    const type = $(e.currentTarget).attr('data-id');
    Session.set('os_when', type);
    Session.set('os_date', null);
    Session.set('os_time', null);
  },
  'change #orderDate'(e) {
    const date = $(e.currentTarget).val();
    Session.set('os_date', date);
    Session.set('os_time', null);
  },
  'click #go-next-step'(e) {
    const r = Session.get('restaurant.public');
    Session.set('order.setup',
      {
        logistic: Session.get('os_logistic'),
        location: Session.get('os_location'),
        destination: Session.get('os_destination'),
        distance: Session.get('os_distance'),
        driving_time: Session.get('os_driving_time'),
        when: Session.get('os_when'),
        date: Session.get('os_date'),
        time: Session.get('os_time')
      }
    );
    Router.go(`/restaurant/store/${r._id}`)
  }
});

function findLocationIndex(id) {
  let index = null;
  const r = Session.get('restaurant.public');
  r.settings.locations.forEach((p, i) => { if (p._id == id ) index = i; });
  return index;
}

function partition(items, size) {
  var result = _.groupBy(items, function(item, i) {
    return Math.floor(i/size);
  });
  return _.values(result);
}