import { GoogleMapsPlaceGetCords } from '../../../../config/async-promises.js';

Template.storeMain.onRendered(function OnRendered() {

  $('#side-menu').metisMenu();

  $('body').addClass('fixed-sidebar');

  $(".navbar-top-store").stick_in_parent();

  $('.sidebar-collapse').slimScroll({
    height: '100%',
    railOpacity: 0.9
  });

  $('.sidebar-container').slimScroll({
    height: '100%',
    railOpacity: 0.4,
    wheelStep: 10
  });

  // AUTO CLOSE RIGHT SIDEBAR IF CLICK OUT OF
  $(document).click(function(event) {
    if(!$(event.target).closest('#right-sidebar').length && !$(event.target).is('#right-sidebar') &&
      !$(event.target).closest('#cart-button').length && !$(event.target).is('#cart-button')) {
      if($('#right-sidebar').hasClass('sidebar-open')) {
        $('#right-sidebar').removeClass('sidebar-open');
      }
    }
  });

  // TOASTR OPTIONS
  toastr.options = {
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true
  };

  $('#contact-form').validate({
    errorPlacement: function (error, element) {
      element.before(error);
    }
  });


});

Template.storeMain.onDestroyed(function OnRendered() {
  $('body').removeClass('fixed-sidebar').removeClass('menuColor');

});

Template.storeMain.onCreated(function OnCreated() {

  const template = Template.instance();

  this.viewData = new ReactiveDict();

  Session.set('order.cart', []);
  Session.set('order.total', 0);
  // CART TOTAL
  Tracker.autorun(() => {
    const cart = Session.get('order.cart');
    // GET TOTAL
    let total = 0;
    cart.forEach((d, i) => {
      total += (d.price * d.quantity);
    });
    Session.set('order.total', Math.round(total * 100) / 100);
  });

  // NORMAL SUBSCRIPTION + STORE VALIDATION
  const rid = Router.current().params.id;

  const handle = this.subscribe('restaurant.public', rid);
  Tracker.autorun(() => {
    const ready = handle.ready();
    if (ready) {

      Session.clear('restaurant.public');
      const r = Restaurant.findOne();
      Session.set('restaurant.public', r);

      // DISABLE
      if (r.billing.disable) history.go(-1) ? history.go(-1) : window.location = 'http://www.google.com';
      else $.unblockUI();

      // SPLIT STORE NAME
      const splitName = r.name.split(" ");
      let splitNameDisplay = "";
      splitName.forEach((p) => { splitNameDisplay = splitNameDisplay + p[0]; });
      $('.split-store-name').html(splitNameDisplay);

      // CREATE GOOGLE MAPS FOR LOCATIONS
      Meteor.setTimeout(() => {
        $("#locations-info-modal").on("shown.bs.modal", function () {
          if (GoogleMaps.loaded()) {
            r.settings.locations.forEach((p, i) => {
              GoogleMaps.create({
                name: `map-${p._id}`,
                element: document.getElementById(`map-${p._id}`),
                options: {
                  center: new google.maps.LatLng(p.map_data.geometry.lat, p.map_data.geometry.lng),
                  zoom: 12
                }
              });
              GoogleMaps.ready(`map-${p._id}`, function(map) {
                // Add a marker to the map once it's ready
                let marker = new google.maps.Marker({
                  position: map.options.center,
                  map: map.instance
                });
              });
            });
          }
        });
      }, 50);

    }
  });


});

Template.storeMain.helpers({
  orderSetup() {
    const orderSetup = Session.get('order.setup');
    if (orderSetup) {
      const loc = findLocation(orderSetup.location);
      orderSetup.location = loc.address;
      return orderSetup;
    }
  },
  // DATA
  r() {
    return Session.get('restaurant.public');
  },
  loc() {
    let order = Session.get('order.setup');
    if (order && order.location) {
      return findLocation(order.location);
    }
  },
  cIcon() {
    const r = Session.get('restaurant.public');
    if (r) {
      return r.settings.region.currency_symbol;
    }
  },
  dishes() {
    const r = Session.get('restaurant.public');
    if (r) {
      const categoryFilter = Template.instance().viewData.get('filter-category');
      if (categoryFilter == "" || !categoryFilter) {
        return r.dishes;
      }
      else {
        return _.filter(r.dishes, function(dish){ return dish.category == categoryFilter; });
      }
    }
  },
  categories() {
    const r = Session.get('restaurant.public');
    if (r) {
      // FIND CATEGORIES WHICH HAVE DISHES
      const categories = _.filter(r.categories, function(cat){
        let exists = false;
        r.dishes.forEach((p) => { if (cat.name == p.category) { exists = true; } });
        return exists;
      });
      return categories;
    }
  },
  checkout() {
    const r = Session.get('restaurant.public');
    const orderSetup = Session.get('order.setup');
    const total = Session.get('order.total');
    const cart = Session.get('order.cart');
    if (r && orderSetup) {
      let error = { allowed: true, message: null };
      if (orderSetup.logistic == 'Delivery' && r.settings.logistic.delivery.conditions.min_order) {
        if (r.settings.logistic.delivery.conditions.min_order > total) {
          error = { allowed: false, message: 'Minimum order for delivery is not met' };
        }
      }
      if (cart.length < 1) {
        error = { allowed: false, message: 'You must have at least 1 item in your cart before you can checkout' };
      }
      return error

    }
  },
  // CATEGORY && HELPER
  categoryFilter() {
    const cat = Template.instance().viewData.get('filter-category');
    if (cat == "" || !cat ) return "All Dishes";
    else return cat
  },
  multi() {
    const r = Session.get('restaurant.public');
    if (r && r.settings.locations)
      return r.settings.locations.length > 1;
  },
  makeArray(obj) {
    return _.map(obj, function(val,key){return {_id: key, times: val}});
  },
  equals(a, b) {
    return a === b;
  },
  // CART FUNCTIONALITY
  cartItems(){
    return Session.get('order.cart');
  },
  cartItemsNum(){
    const cart = Session.get('order.cart');
    let total = 0;
    cart.forEach((p, i) => {
      total += p.quantity;
    });
    return total;
  },
  cartTotal() {
    return Session.get('order.total');
  },
  cartDishPrice(d) {
    return Math.round(d.price * 100) / 100;
  },
  cartDishPriceQty(d) {
    let price = d.price * d.quantity;
    return Math.round(price * 100) / 100;
  },
  addOne(i) {
    return i + 1;
  },
  greater(a, b) {
    return a > b;
  },
});

Template.storeMain.events({
  'submit #checkout-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const r = Session.get('restaurant.public');
    const config = Session.get('order.setup');

    config.payment = {
      method: $('input[name="paymentMethod"]:checked').val(),
      captured: null
    };

    const order = {
      restaurant_id: r._id,
      total: Session.get('order.total'),
      customer: {
        name: $('#orderName').val(),
        phone: $('#orderPhone').val(),
        email: $('#orderEmail').val()
      },
      config: config,
      dishes: Session.get('order.cart'),
    };

    if (config.payment.method == 'Online') {
      StripeCheckout.open({
        image: r.settings.design.images.logo.url,
        locale: 'auto',
        key: Meteor.settings.public.stripe,
        name: r.name,
        description: 'Online Order',
        currency: r.settings.payment.stripe.default_currency,
        amount: order.total * 100,
        zipcode: true,
        email: order.customer.email,
        token(token) {
          console.log(token);
          Meteor.call('createOrderStripe', order, token, (err, res) => {
            if (err) toastr.error(err, 'Error');
            else {
              Router.go(`/order/${res}/${r._id}`);
            }
            $.unblockUI();
          });
        }
      });
    }
    else {
      Meteor.call('createOrder', order, (err, res) => {
        if (err) toastr.error(err, 'Error');
        else Router.go(`/order/${res}/${r._id}`);
        $.unblockUI();
      });
    }
  },
  'submit #contact-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#contact-form').valid();
    if (formValid) {
      const r = Session.get('restaurant.public');
      const message = {
        name: $('#contactName').val(),
        contact: $('#contactMethod').val(),
        message: $('#contactMessage').val()
      };
      Meteor.call('sendStoreContactEmail', message, r._id, (err) => {
        if (err) toastr.error(err, 'Error');
        else {
          $('#contact-info-modal').modal('hide');
          $('#contactName').val('');
          $('#contactMethod').val('');
          $('#contactMessage').val('');
          toastr.success('Message Sent')
        }
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .category-filter'(e, ins) {
    const id = $(e.currentTarget).attr('data-id');
    ins.viewData.set('filter-category', id);
  },
  'click #cart-button'() {
    //if ($('body').hasClass('mini-navbar')) $('body').removeClass('mini-navbar');
    $('.navbar-collapse').removeClass('in');
    $('#right-sidebar').toggleClass('sidebar-open');
  },
  'click .info-modal'(e) {
    const id = $(e.currentTarget).attr('data-id');
    $(`#${id}-info-modal`).modal('show');
  },
  'click .add-dish'(e, ins) {
    const id = $(e.currentTarget).attr('data-id');
    const dish = findDish(id);
    ins.viewData.set('add-dish-modal-id', id);
    // CHECK CART FOR DUPLICATES
    if (dish.option_sets.length > 0) {
      const r = Session.get('restaurant.public');
      $('#add-modal-dish-image').attr('src', dish.image.url);
      let headString = ``;
      if (dish.image.rel) {
        headString += `<div class="row">
                          <div class="col-sm-3">
                            <img src="${dish.image.url}" class="res-img">
                          </div>
                          <div class="col-sm-9">
                            <div class="l-r-flex" class="text-right" style="align-items: flex-start;">
                              <div class="text-left" style="width:75%;">
                                  <h4 class="modal-title">${dish.name}</h4>
                                  <p class="font-bold">${dish.category}</p>
                                  <p class="">${dish.description}</p>
                              </div>
                              <h4 class="modal-title" id="add-modal-dish-price">${r.settings.region.currency_symbol}${dish.price}</h4>
                            </div>
                          </div>
                        </div>`;
      }
      else {
        headString += `<div class="l-r-flex" class="text-right" style="align-items: flex-start;">
                          <div class="text-left" style="width:75%;">
                              <h4 class="modal-title">${dish.name}</h4>
                              <p class="font-bold">${dish.category}</p>
                              <p class="">${dish.description}</p>
                          </div>
                          <h4 class="modal-title" id="add-modal-dish-price">${r.settings.region.currency_symbol}${dish.price}</h4>
                        </div>`;
      }
      $('#modal-dish-image-container').empty().append(headString);
      $('#add-modal-dish-quantity').val(1);

      let htmlString = ``;
      dish.option_sets.forEach((p, i) => {
        if (p.required && p.multi_select)
          htmlString += `<label id="${dish._id}-${p._id}-header">${p.name} (Required / Select Multiple)</label>`;
        else if (p.required && !p.multi_select)
          htmlString += `<label id="${dish._id}-${p._id}-header">${p.name} (Required / Select One)</label>`;
        else if (p.multi_select && !p.required)
          htmlString += `<label id="${dish._id}-${p._id}-header">${p.name} (Optional / Select Multiple)</label>`;
        else
          htmlString += `<label id="${dish._id}-${p._id}-header">${p.name} (Optional / Select One)</label>`;

        htmlString +=  `<ul class="todo-list m-b small-list">`;

        p.options.forEach((o, k) => {
          if (p.multi_select) {
            if (o.modifier)
              htmlString += `<li><div class="checkbox checkbox-primary checkbox-inline"><input id="${dish._id}-${p._id}-${o._id}" class="${dish._id}-${p._id}" type="checkbox"><label for="${dish._id}-${p._id}-${o._id}"> ${o.name} + ${r.settings.region.currency_symbol}${o.modifier} </label></div></li>`;
            else
              htmlString += `<li><div class="checkbox checkbox-primary checkbox-inline"><input id="${dish._id}-${p._id}-${o._id}" class="${dish._id}-${p._id}" type="checkbox"><label for="${dish._id}-${p._id}-${o._id}"> ${o.name} </label></div></li>`;
          }
          else {
            if (o.modifier)
              htmlString += `<li><div class="radio radio-primary radio-inline"><input id="${dish._id}-${p._id}-${o._id}" class="${dish._id}-${p._id}" type="radio" name="${dish._id}-${p._id}"><label for="${dish._id}-${p._id}-${o._id}"> ${o.name} + ${r.settings.region.currency_symbol}${o.modifier} </label></div></li>`;
            else
              htmlString += `<li><div class="radio radio-primary radio-inline"><input id="${dish._id}-${p._id}-${o._id}" class="${dish._id}-${p._id}" type="radio" name="${dish._id}-${p._id}"><label for="${dish._id}-${p._id}-${o._id}"> ${o.name} </label></div></li>`;
          }
        });
        htmlString += `</ul>`

      });
      $('#add-modal-dish-options').empty();
      $('#add-modal-dish-options').append(htmlString);
      $('#add-dish-modal').modal('show');
    }
    else {
      addDish(dish);
      toastr.success('Dish added to cart');
    }
  },
  'click .add-dish-modal-button'(e, ins) {
    const id = ins.viewData.get('add-dish-modal-id');
    const dish = findDish(id);
    const quantity = parseInt($('#add-modal-dish-quantity').val(), 10);
    if (quantity <= 0) toastr.error('Enter a quantity of 1 or above');
    else {
      let allG = true;
      // CHECK WHICH OPTIONS ARE SET AND THROW ERRORS IF NOT SET
      dish.option_sets.forEach((s, i) => {
        let ok = false;
        if (s.required) {
          $(`.${dish._id}-${s._id}`).each(function(e) {
            if ($(this).prop('checked') == true) ok = true;
          });
        }
        else {
          ok = true;
        }
        if (!ok) {
          $(`#${dish._id}-${s._id}-header`).addClass('text-danger');
          allG = false;
        }
        else {
          $(`#${dish._id}-${s._id}-header`).removeClass('text-danger');
        }
      });

      if (allG) {
        // Check options that are set
        dish.option_sets.forEach((s, i) => {
          s.options.forEach((o, k) => {
            let toSet = $(`#${dish._id}-${s._id}-${o._id}`).prop('checked');
            if (toSet) dish.price += o.modifier;
            o.isSet = toSet;
          });
        });
        dish.price = Math.round(dish.price * 100) / 100;
        // ADD DISH
        addDish(dish);
        $('#add-dish-modal').modal('hide');
        toastr.success('Dish added to cart');
      }
      else {
        toastr.error('Please select from the required options');
      }
    }
  },
  'click .remove-dish'(e) {
    const id = $(e.currentTarget).attr('data-id');
    const cart = Session.get('order.cart');
    let index = null;
    cart.forEach((p, i) => { if (p._id == id) index = i });
    cart.splice(index, 1);
    Session.set('order.cart', cart);
  },
  'click .start-order'() {
    const r = Session.get('restaurant.public');
    Router.go(`/restaurant/${r._id}`)
  }
});


function addDishV2(dish) {
  let index = null;
  const cart = Session.get('order.cart');
  cart.forEach((p, i) => { if (p._id == dish._id) index = i });
  if (index || index == 0) {
    cart[index].quantity += 1;
    Session.set('order.cart', cart);
  }
  else {
    dish.quantity = 1;
    cart.push(dish);
    Session.set('order.cart', cart);
  }
}

function addDish(dish) {
  const cart = Session.get('order.cart');
  // CHECK FOR DUPLICATES
  // SEE IF HAS OPTION SETS OR NOT

  if (dish.option_sets.length > 0) {
    const quantity = parseInt($('#add-modal-dish-quantity').val(), 10);
    const index = [];
    cart.forEach((p, i) => { if (p._id == dish._id) index.push(i); });

    if (index.length > 0) {
      // REDUCE DISH
      const dishReduced = [];
      dish.option_sets.forEach((s, i) => {
        s.options.forEach((o, k) => {
          if (o.isSet) dishReduced.push(true);
          else dishReduced.push(false);
        });
      });
      // REDUCE THE CART MATCHES
      const cartDishes = [];
      index.forEach((p) => { cartDishes.push(cart[p]); });
      const cartDishesReduced = [];
      cartDishes.forEach((d, y) => {
        cartDishesReduced[y] = [];
        d.option_sets.forEach((s, i) => {
          s.options.forEach((o, k) => {
            if (o.isSet) cartDishesReduced[y].push(true);
            else cartDishesReduced[y].push(false);
          });
        });
      });
      // COMPARE TO SEE IF ONE MATCHES
      let match = null;
      cartDishesReduced.forEach((p, i) => {
        let found = true;
        p.forEach((r, k) => {
          if (r !== dishReduced[k]) {
            found = false;
          }
        });
        if (found) match = i;
      });
      if (match || match == 0) {
        const dIndex = index[match];
        cart[dIndex].quantity += quantity;
        Session.set('order.cart', cart);
      }
      else {
        dish.quantity = quantity;
        cart.push(dish);
        Session.set('order.cart', cart);
      }
    }
    // NONE SAME
    else {
      dish.quantity = quantity;
      cart.push(dish);
      Session.set('order.cart', cart);
    }
  }
  else {
    let index = null;
    cart.forEach((p, i) => { if (p._id == dish._id) index = i });
    if (index || index == 0) {
      cart[index].quantity += 1;
      Session.set('order.cart', cart);
    }
    else {
      dish.quantity = 1;
      cart.push(dish);
      Session.set('order.cart', cart);
    }

  }
}

function findDish(id) {
  let index = null;
  const r = Session.get('restaurant.public');
  if (r) {
    r.dishes.forEach((p, i) => { if (p._id == id ) index = i; });
    return r.dishes[index];
  }
  else {
    return null
  }
}

function findLocation(id) {
  let index = null;
  const r = Session.get('restaurant.public');
  if (r) {
    r.settings.locations.forEach((p, i) => { if (p._id == id ) index = i; });
    return r.settings.locations[index];
  }
}