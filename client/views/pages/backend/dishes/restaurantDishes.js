import { S3UploadePromise, S3DeletePromise } from '../../../../config/async-promises.js';

Template.restaurantDishes.onRendered(function () {

  // DISH FORM
  $('.dish-form').validate({
    errorPlacement: function (error, element) {
      element.before(error);
    }
  });

});

Template.restaurantDishes.onCreated(function () {

  this.viewData = new ReactiveDict();

// Create Subscriptions
  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        Session.set('restaurant.private', r);
        $(".select2").select2();
        // DISHES TABLE

        const cIcon = r.settings.region.currency_symbol;

        const formatted_dishes = [];

        r.dishes.forEach((p) => {
          let dish = {};
          dish.name = p.name;
          dish.price = `${cIcon}${p.price}`;
          dish.category = p.category;
          dish.action = `<div class="btn-group dropup">\
                      <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></button>\
                      <ul class="dropdown-menu dropdown-menu-right">\
                      <li><a data-id="${p._id}" class="edit-dish">Edit</a></li>\
                      <li><a data-id="${p._id}" class="delete-dish">Delete</a></li>\
                      </ul>\
                      </div>`;

          dish.description = p.description;

          let optionsets = ``;

          if (p.option_sets.length > 0) {
            p.option_sets.forEach((os, k) => {
              let required = os.required ? '(Required)' : '';
              let multi = os.multi_select ? '(Multi-Select)' : '';
              optionsets += `<p>${os.name} ${required} ${multi}</p><ul class="p-l-sm">`;
              os.options.forEach((o) => {
                if (o.modifier) optionsets += `<li>${o.name} - ${cIcon}${o.modifier}</li>`;
                else optionsets += `<li>${o.name}</li>`;
              })
              optionsets += `</ul>`;
            });
          }
          else optionsets = '<p class="m-b-none">No option sets</p>';

          dish.optionsets = optionsets;

          let image = ``;
          if (p.image.rel) image = `<img class="thumbnail-img" src="${p.image.url}"/>`;
          else image = `<p class="m-b-none">No image</p>`;
          dish.image = image;

          formatted_dishes.push(dish);
        });

        $('#table').remove();
        $('#table-container').append(`<table id="table" class="table" data-sorting="true"></table>`);
        $('#table').footable({
          "filtering": {
            "enabled": true
          },
          "paging": {
            "enabled": true
          },
          columns: [
            { name: 'name', title: 'Name' },
            { name: 'price', title: 'Price', breakpoints: 'xs' },
            { name: 'category', title: 'Category', breakpoints: 'xs' },
            { name: 'action', title: 'Action', type: 'html' },
            { name: 'description', title: 'Description', breakpoints: 'all' },
            { name: 'optionsets', title: 'Option Sets', type: 'html', breakpoints: 'all' },
            { name: 'image', title: 'Image', type: 'html', breakpoints: 'all' }
          ],
          rows: formatted_dishes
        });

        let filtering = FooTable.get('#table').use(FooTable.Filtering);
        let catFilter = $('#filterCategory').val();
        filtering.addFilter('category', catFilter, ['category']);
        let searchFilter = $('#filterSearch').val();
        filtering.addFilter('all', searchFilter, ['name', 'price', 'category', 'description', 'optionsets']);
        filtering.filter();

        Meteor.setTimeout(() => {$('.select2').select2()}, 15);


      }
    }
  });

});

Template.restaurantDishes.helpers({
  r() {
    return Session.get('restaurant.private')
  }
});

Template.restaurantDishes.events({
  'submit #create-dish-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#create-dish-form').valid();
    if (formValid) {
      const r = Session.get('restaurant.private');
      const dish = {
        name: $('#dishName').val(),
        price: Math.round($('#dishPrice').val() * 100) / 100,
        description: $('#dishDescription').val(),
        category: $('#dishCategory').val(),
        image: {
          url: null,
          rel: null
        },
        option_sets: []
      };

      let options = $('#dishOptionSets').val();
      if (options && options.length > 0) {
        r.option_sets.forEach((p, i) => {
          options.forEach((o, k) => {
            if (p._id === o) dish.option_sets.push(p)
          });
        });
      }

      const dishImage = $('input.dish-image')[0].files;
      const dishPromise = S3UploadePromise(dishImage, 100000, `images/${r._id}/dishes`);
      dishPromise.then((res) => {
        if (res !== 'No File') {
          dish.image.url = res.secure_url;
          dish.image.rel = res.relative_url;
        }
        Meteor.call('createDish', dish, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#dishName').val('');
            $('#dishPrice').val('');
            $('#dishDescription').val('');
            $('#dishImage').val('');
            $('#dishOptionSets').val(null);
            $.unblockUI();
            toastr.success('Dish Created');
          }

        });
      }).catch((err) =>{
        toastr.error(err, "Error");
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .deleteDishImage'(e, ins) {
    $.blockUI({ message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const id = ins.viewData.get('edit-dish-id');
    const image = ins.viewData.get('editDishImageData');
    const deleteImage = S3DeletePromise(image.rel);
    deleteImage.then(() => {
      Meteor.call('deleteDishImage', id, (err) => {
        if (err) toastr.error(err, 'Error');
        else {
          toastr.success('Image removed');
          $('#existingDishImage').html(`<p>No image</p>`);
        }
        $.unblockUI();
      });
    });
  },
  'click .edit-dish'(e, ins) {
    const $this = $(e.currentTarget);
    const id = $this.attr('data-id');
    const r = Session.get('restaurant.private');
    let index = 0;
    r.dishes.forEach((p, i) => { if (p._id == id) index = i; });
    ins.viewData.set('edit-dish-id', id);
    $('#edit-dish-modal').modal('show');
    $('#editDishName').val(r.dishes[index].name);
    $('#editDishPrice').val(r.dishes[index].price);
    $('#editDishDescription').val(r.dishes[index].description);
    $('#editDishCategory').val(r.dishes[index].category).trigger('change');
    let options = [];
    r.dishes[index].option_sets.forEach((o, i) => { options.push(o._id); });
    if (options.length > 0) $('#editDishOptionSets').val(options).trigger('change');
    $('#existingDishImage').html(``);
    if (r.dishes[index].image.rel) {
      $('#existingDishImage').html(`<img class="thumbnail-img" src="${r.dishes[index].image.url}"/><br>\
          <button type="button" class="btn btn-warning btn-xs m-t-xs deleteDishImage">Remove Image</button>`);
    }
    else {
      $('#existingDishImage').html(`<p>No image</p>`);
    }
    ins.viewData.set('editDishImageData', { url: r.dishes[index].image.url, rel: r.dishes[index].image.rel});
    $('.select2').select2();
  },
  'click .edit-dish-modal-button'(e, ins) {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#edit-dish-form').valid();
    if (formValid) {
      const r = Session.get('restaurant.private');

      const dish = {
        _id: ins.viewData.get('edit-dish-id'),
        name:  $('#editDishName').val(),
        price: Math.round($('#editDishPrice').val() * 100) / 100,
        description: $('#editDishDescription').val(),
        category: $('#editDishCategory').val(),
        image: ins.viewData.get('editDishImageData'),
        option_sets: []
      };
      let options = $('#editDishOptionSets').val();
      if (options && options.length > 0) {
        r.option_sets.forEach((p, i) => {
          options.forEach((o, k) => {
            if (p._id === o) dish.option_sets.push(p)
          });
        });
      }
      const dishImage = $('#editDishImage')[0].files;
      const dishPromise = S3UploadePromise(dishImage, 100000, 'images/dishes');
      let dishDeletePromise = S3DeletePromise(null);
      if (dishImage.length > 0)
        dishDeletePromise = S3DeletePromise(dish.image.rel);

      Promise.all([dishPromise, dishDeletePromise]).then((vals) => {
        // 0 - STATIC RESTAURANT DATA - 1 - LOGO - 2 - BACKGROUND
        if (vals[0] !== 'No File') {
          dish.image.url = vals[0].secure_url;
          dish.image.rel = vals[0].relative_url;
        }
        Meteor.call('editDish', dish, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#edit-dish-modal').modal('hide');
            $('#editDishName').val('');
            $('#editDishPrice').val('');
            $('#editDishDescription').val('');
            $('#editDishCategory').val('');
            $('#editDishOptionSets').val(null);
            $('#existingDishImage').html('');
            $('#editDishImage').val('');
            toastr.success('Dish Edited');
          }
          $.unblockUI();
        });
      }).catch((err) => {
        toastr.error(err, 'Error');
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .delete-dish'(e) {
    swal({
      title: "Delete Dish?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: true,
    }, function() {
      const id = $(e.currentTarget).attr('data-id');
      Meteor.call('deleteDish', id, (err) => {
        if (err) toastr.error(err, "Error");
        else toastr.success('Dish Deleted');
      });
    });
  },
  'change #filterCategory'(e) {
    let filtering = FooTable.get('#table').use(FooTable.Filtering), // get the filtering component for the table
      filter = $('#filterCategory').val(); // get the value to filter by
    if (filter === ''){ // if the value is "none" remove the filter
      filtering.removeFilter('category');
      filtering.clear();
    } else { // otherwise add/update the filter.
      filtering.addFilter('category', filter, ['category']);
      filtering.filter();
    }
  },
  'change #filterSearch'(e) {
    let filtering = FooTable.get('#table').use(FooTable.Filtering), // get the filtering component for the table
      filter = $('#filterSearch').val(); // get the value to filter by
    if (filter === ''){ // if the value is "none" remove the filter
      filtering.removeFilter('all');
      filtering.clear();
    } else { // otherwise add/update the filter.
      filtering.addFilter('all', filter, ['name', 'price', 'category', 'description', 'optionsets']);
      filtering.filter();
    }
  },
});
