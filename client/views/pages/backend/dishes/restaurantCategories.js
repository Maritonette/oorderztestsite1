Template.restaurantCategories.onRendered(function () {

  $('.create-category-form').validate({
    errorPlacement: function (error, element) {
      element.before(error);
    }
  });

});

Template.restaurantCategories.onCreated(function () {

  this.viewData = new ReactiveDict();

// Create Subscriptions
  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        Session.set('restaurant.private', r);
        // CATEGORY TABLE
        const cats = r.categories;
        $('#category-table').remove();
        cats.forEach((p) => {
          p.actionHtml = `<div class="btn-group dropup">\
                      <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></button>\
                      <ul class="dropdown-menu dropdown-menu-right">\
                      <li><a data-id="${p._id}" class="edit-category">Edit</a></li>\
                      <li><a data-id="${p._id}" class="delete-category">Delete</a></li>\
                      </ul>\
                      </div>`;
        });
        $('#category-table-container').append('<table class="table m-b-none" id="category-table" data-sorting="true"></table>');
        $('#category-table').footable({
          "columns": [
            { "name" : "name", "title" : "Name" },
            { "name" : "actionHtml", "title" : "Action", "type": "html"}
          ],
          "rows": cats
        });
      }
    }
  });

});

Template.restaurantCategories.helpers({
  r() {
    return Session.get('restaurant.private')
  }
});

Template.restaurantCategories.events({
  'submit #create-category-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#create-category-form').valid();
    if (formValid) {

      const categoryVal = $('#categoryName').val();
      const r = Session.get('restaurant.private');

      // CHECK FOR ERRORS
      let catExists = false;
      r.categories.forEach((p) => { if (p.name == categoryVal) catExists = true; });
      if (catExists) {
        toastr.error('Category with this name already exists', 'Error');
        $.unblockUI();
      }
      else {
        // NO ERRORS BUILD THE CAT
        const category = {
          name: categoryVal
        };
        Meteor.call('createCategory', category, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#categoryName').val('');
            $.unblockUI();
            toastr.success('Category Created');
          }
        });
      }

    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .edit-category'(e, ins) {
    const $this = $(e.currentTarget);
    const id = $this.attr('data-id');
    const r = Session.get('restaurant.private');
    let catIndex = 0;
    r.categories.forEach((p, i) => { if (p._id == id) catIndex = i; });
    ins.viewData.set('edit-category-id', id);
    $('#edit-category-modal').modal('show');
    $('#editCategoryName').val(r.categories[catIndex].name);
  },
  'click .edit-category-modal-button'(e, ins) {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#edit-category-form').valid();
    if (formValid) {
      const id = ins.viewData.get('edit-category-id');
      const categoryVal = $('#editCategoryName').val();
      const r = Session.get('restaurant.private');
      // CHECK FOR ERRORS
      let catExists = false;
      r.categories.forEach((p, i) => {
        if (p.name == categoryVal && p._id !== id) catExists = true;
      });
      if (catExists) {
        toastr.error('Category with this name already exists', 'Error');
        $.unblockUI();
      }
      else {
        const category = {
          _id: id,
          name: categoryVal
        };
        Meteor.call('editCategory', category, id, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#edit-category-modal').modal('hide');
            $('#categoryName').val('');
            $.unblockUI();
            toastr.success('Category Edited');
          }
        });
      }
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .delete-category'(e) {
    swal({
      title: "Delete Category?",
      text: "Deleting a category will not affect your dishes but it will no longer show up under your shop or dashboard filters",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: true,
    }, function() {
      const $this = $(e.currentTarget);
      const category = $this.attr('data-id');
      Meteor.call('deleteCategory', category, (err) => {
        if (err) toastr.error(err, "Error");
      });
    });
  },
});
