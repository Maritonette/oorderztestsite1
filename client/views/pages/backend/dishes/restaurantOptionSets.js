Template.restaurantOptionSets.onRendered(function () {

  $('.select2').select2();

  $('.create-option-set-form').validate({
    errorPlacement: function (error, element) {
      element.before(error);
    }
  });

  $('.create-option-form').validate({
    errorPlacement: function (error, element) {
      element.before(error);
    }
  });

});

Template.restaurantOptionSets.onCreated(function () {

  this.viewData = new ReactiveDict();

// Create Subscriptions
  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        Session.set('restaurant.private', r);

        let formatted_option_sets = [];

        r.option_sets.forEach((p, i) => {

          let option_set = {};
          option_set.name = p.name;
          option_set.required = p.required ? 'Yes' : 'No';
          option_set.multi_select = p.multi_select ? 'Yes' : 'No';

          option_set.options = ``;
          if (p.options.length > 0) {
            p.options.forEach((option, k) => {
              if (option.modifier) option_set.options += `<p class="m-b-xs"><span data-id="${option._id}" data-set="${p._id}" class="badge badge-danger m-r-sm delete-option cursor"><i class="fa fa-ban"></i></span>${option.name} - ${r.settings.region.currency_symbol}${option.modifier}</p>`;
              else option_set.options += `<p class="m-b-xs"><span data-id="${option._id}" data-set="${p._id}" class="badge badge-danger m-r-sm delete-option cursor"><i class="fa fa-ban"></i></span>${option.name}</p>`
            });
            option_set.options += ``
          }
          else option_set.options = `<p class="m-b-none">No options</p>`;

          option_set.action = `<div class="btn-group dropup">\
                      <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></button>\
                      <ul class="dropdown-menu dropdown-menu-right">\
                      <li><a data-id="${p._id}" class="edit-option-set">Edit</a></li>\
                      <li><a data-id="${p._id}" class="delete-option-set">Delete</a></li>\
                      </ul>\
                      </div>`;

          formatted_option_sets.push(option_set);

        });

        $('#table').remove();
        $('#table-container').append(`<table id="table" class="table" data-sorting="true"></table>`);
        $('#table').footable({
          "filtering": {
            "enabled": true
          },
          "paging": {
            "enabled": true
          },
          columns: [
            { name: 'name', title: 'Name' },
            { name: 'required', title: 'Required', breakpoints: 'xs' },
            { name: 'multi_select', title: 'Multi Select', breakpoints: 'xs' },
            { name: 'action', title: 'Action', type: 'html' },
            { name: 'options', title: 'Options', type: 'html', breakpoints: 'all' }
          ],
          rows: formatted_option_sets
        });

        Meteor.setTimeout(() => {$('.select2').select2()}, 15);

      }
    }
  });

});

Template.restaurantOptionSets.helpers({
  r() {
    return Session.get('restaurant.private')
  }
});

Template.restaurantOptionSets.events({
  'submit #create-option-set-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#create-option-set-form').valid();
    if (formValid) {

      const option_set = {
        name: $('#optionSetName').val(),
        required: $('#optionSetRequired').prop('checked'),
        multi_select: $('#optionSetMultiSelect').prop('checked'),
        options: []
      };

      const r = Session.get('restaurant.private');

      // CHECK FOR ERRORS
      let osExists = false;
      r.option_sets.forEach((p) => { if (p.name == option_set.name) osExists = true; });
      if (osExists) {
        toastr.error('Option set with this name already exists', 'Error');
        $.unblockUI();
      }
      else {
        Meteor.call('createOptionSet', option_set, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#optionSetName').val('');
            $('optionSetRequired').prop('checked', false);
            $('#optionSetMultiSelect').prop('checked', false);
            $.unblockUI();
            toastr.success('Option Set Created');
          }
        });
      }

    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .edit-option-set'(e, ins) {
    const id = $(e.currentTarget).attr('data-id');
    const r = Session.get('restaurant.private');
    let osIndex = 0;
    r.option_sets.forEach((p, i) => { if (p._id == id) osIndex = i; });
    ins.viewData.set('edit-option-set-id', id);
    $('#edit-option-set-modal').modal('show');
    $('#editOptionSetName').val(r.option_sets[osIndex].name);
    $('#editOptionSetRequired').prop('checked', r.option_sets[osIndex].required);
    $('#editOptionSetMultiSelect').prop('checked', r.option_sets[osIndex].multi_select);
  },
  'click .edit-option-set-modal-button'(e, ins) {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#edit-option-set-form').valid();
    if (formValid) {
      const r = Session.get('restaurant.private');
      const option_set = {
        _id: ins.viewData.get('edit-option-set-id'),
        name: $('#editOptionSetName').val(),
        required: $('#editOptionSetRequired').prop('checked'),
        multi_select: $('#editOptionSetMultiSelect').prop('checked')
      };
      let osIndex = null;
      r.option_sets.forEach((p, i) => { if (p._id == option_set._id) osIndex = i; });
      option_set.options = r.option_sets[osIndex].options;
      // CHECK FOR ERRORS
      let osExists = false;
      r.option_sets.forEach((p) => { if (p.name == option_set.name && p._id !== option_set._id) osExists = true; });
      if (osExists) {
        toastr.error('Option set with this name already exists', 'Error');
        $.unblockUI();
      }
      else {
        Meteor.call('editOptionSet', option_set, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#edit-option-set-modal').modal('hide');
            $('#editOptionSetName').val('');
            $('#editOptionSetRequired').prop('checked', false);
            $('#editOptionSetMultiSelect').prop('checked', false);
            $.unblockUI();
            toastr.success('Option Set Edited');
          }
        });
      }
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .delete-option-set'(e) {
    swal({
      title: "Delete Option Set?",
      text: "Deleting an option set will not not update the dishes currently using them",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: true,
    }, function() {
      const setId = $(e.currentTarget).attr('data-id');
      Meteor.call('deleteOptionSet', setId, (err) => {
        if (err) toastr.error(err, "Error");
        else toastr.success('Option set deleted');
      });
    });
  },
  'submit #create-option-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#create-option-form').valid();
    if (formValid) {

      const setId = $('#optionAddToSet').val();
      const option = {
        name: $('#optionName').val(),
        modifier: $('#optionModifier').val() ? Math.round($('#optionModifier').val() * 100) / 100 : null,
        isSet: null
      };
      const r = Session.get('restaurant.private');

      // CHECK FOR ERRORS
      let osIndex = false;
      r.option_sets.forEach((p, i) => { if (p._id == setId) osIndex = i; });
      let oExists = null;
      r.option_sets[osIndex].options.forEach((p, i) => { if (p.name == option.name) oExists = true });
      if (oExists) {
        toastr.error(`Option with this name already exists in "${r.option_sets[osIndex].name}"`, 'Error');
        $.unblockUI();
      }
      else {
        Meteor.call('createOption', option, setId, (err) => {
          if (err) toastr.error(err, 'Error');
          else {
            $('#optionName').val('');
            $('#optionModifier').val('');
            $.unblockUI();
            toastr.success('Option Created');
          }
        });
      }

    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .delete-option'(e) {
    e.preventDefault();
    swal({
      title: "Delete Option",
      text: "Deleting an option will not not update the dishes currently using them",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: true,
    }, function() {
      const id = $(e.currentTarget).attr('data-id');
      const setId = $(e.currentTarget).attr('data-set');
      Meteor.call('deleteOption', id, setId, (err) => {
        if (err) toastr.error(err, "Error");
        else toastr.success('Option deleted');
      });
    });
  },
});
