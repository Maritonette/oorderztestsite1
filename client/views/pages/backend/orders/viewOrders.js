Template.restaurantViewOrders.onRendered(function () {
  $('.select2').select2();
});

Template.restaurantViewOrders.onCreated(function () {

  this.viewData = new ReactiveDict();

// Create Subscriptions
  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        Session.set('restaurant.private',r );
      }
    }
  });

  // Create Subscriptions
  const o_handle = this.subscribe('orders.private');
  Tracker.autorun(() => {
    const ready = o_handle.ready();
    if (ready) {
      const orders = Order.find().fetch();
      if (orders) {
        const r = Session.get('restaurant.private');
        if (r) {
          let o = orders;
          // SORT ORDERS BY DATE CREATED, OLDEST FIRST
          o = _.sortBy(o, (orders) => { return -orders.created; });

          const cIcon = r.settings.region.currency_symbol;
          
          const formatted_orders = [];

          o.forEach((p, i) => {

            let orderNumber = o.length - i;

            //let now = moment.utc(Date.now()).tz(r.settings.region.timezone);
            let created = moment.utc(p.created).tz(r.settings.region.timezone);
            let placed = `${created.format('h:m A')} - ${created.format('DD/MM/YYYY')}`;

            let status = null;

            if (p.status == "Cancelled") status = `<span class="label label-danger">Cancelled</span>`;
            if (p.status == "Un-Confirmed") status = `<span class="label label-warning">Un-Confirmed</span>`;
            if (p.status == "Confirmed") status = `<span class="label label-info">Confirmed</span>`;
            if (p.status == "Ready") status = `<span class="label label-info">Ready</span>`;
            if (p.status == "On Route") status = `<span class="label label-info">On Route</span>`;
            if (p.status == "Complete") status = `<span class="label label-success">Complete</span>`;

            let destination = p.config.destination ? p.config.destination : 'N/A';
            let distance = p.config.distance ? `${(p.config.distance / 1000)} km` : 'N/A';
            let drivingTime = p.config.driving_time ? `${Math.round((p.config.driving_time / 60) * 100) / 100} minutes` : 'N/A';

            let paymentCaptured = null;
            if (p.config.payment.method == 'Online') paymentCaptured = p.config.payment.captured ? `Yes` : 'No';
            else paymentCaptured ='N/A';

            let items = `<ul class="p-l-sm">`;
            p.dishes.forEach((d, k) => {
              items += `<li>${d.name} x ${d.quantity} / Qty</li>`;

              if (d.option_sets && d.option_sets.length > 0) {
                let atLeastOne = null;
                d.option_sets.forEach((os) => {
                  os.options.forEach((o) => {
                    if (o.isSet) atLeastOne = true;
                  })
                });

                if (atLeastOne) {
                  items += `<ul>`;
                  d.option_sets.forEach((os) => {
                    os.options.forEach((o) => {
                      if (o.isSet) items += `<li>${os.name} / ${o.name}</li>`;
                    })
                  });
                  items += `</ul>`;
                }
              }

            });
            items += `</ul>`;


            let actionHtml = `<div class="btn-group dropup">\
                      <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></button>\
                      <ul class="dropdown-menu dropdown-menu-right">`;
            if (p.status == "On Route") {
              actionHtml += `<li><a data-id="${p._id}" class="update-complete">Complete</a></li>`;
            }
            if (p.status == "Ready") {
              actionHtml += `<li><a data-id="${p._id}" class="update-complete">Complete</a></li>`;
              if (p.config.logistic == 'Delivery') actionHtml += `<li><a data-id="${p._id}" class="update-on-route">On Route</a></li>`;
            }
            if (p.status == "Confirmed") {
              actionHtml += `<li><a data-id="${p._id}" class="update-complete">Complete</a></li>`;
              if (p.config.logistic == 'Delivery') actionHtml += `<li><a data-id="${p._id}" class="update-on-route">On Route</a></li>`;
              actionHtml += `<li><a data-id="${p._id}" class="update-ready">Ready</a></li>`;
              actionHtml += `<li><a data-id="${p._id}" class="update-unconfirmed">Un-confirm</a></li>`;
            }
            if (p.status == "Un-Confirmed") {
              actionHtml += `<li><a data-id="${p._id}" class="update-confirmed">Confirm</a></li>`;
            }
            if (p.status !== "Cancelled") {
              actionHtml += `<li><a data-id="${p._id}" class="update-cancelled">Cancel</a></li>`;
            }
            if (p.status == "Cancelled") {
              actionHtml += `<li><a data-id="${p._id}" class="update-confirmed">Confirm</a></li>`;
              actionHtml += `<li><a data-id="${p._id}" class="update-unconfirmed">Un-Confirm</a></li>`;
            }
            actionHtml += `<li><a href="/order/${p._id}/${r._id}" target="_blank">Receipt</a></li>`;
            actionHtml += `</ul></div>`;
            
            formatted_orders.push({
              no: orderNumber,
              placed: placed,
              status: status,
              name: p.customer.name,
              phone: p.customer.phone,
              email: p.customer.email,
              total: `${cIcon}${p.total}`,
              payment_method: p.config.payment.method,
              payment_captured: paymentCaptured,
              location: p.config.location,
              due: p.config.when,
              logistic: p.config.logistic,
              destination: destination,
              distance: distance,
              driving_time: drivingTime,
              action: actionHtml,
              items: items
            });
          });


          $('#table').remove();
          $('#table-container').append(`<table id="table" class="table" data-sorting="true"></table>`);
          $('#table').footable({
            "filtering": {
              "enabled": true
            },
            "paging": {
              "enabled": true
            },
            columns: [
              { name: 'no', title: 'No.'},
              { name: 'placed', title: 'Placed', breakpoints: 'xs'},
              { name: 'status', title: 'Status'},
              { name: 'total', title: 'Total', breakpoints: 'xs'},
              { name: 'name', title: 'Name', breakpoints: 'xs sm'},
              { name: 'phone', title: 'Phone', breakpoints: 'xs sm md'},
              { name: 'email', title: 'E-Mail', breakpoints: 'xs sm md lg' },
              { name: 'payment_method', title: 'Payment', breakpoints: 'xs sm md'},
              { name: 'payment_captured', title: 'Pay. Received', breakpoints: 'xs sm md lg xl'},
              { name: 'location', title: 'Store', breakpoints: 'all', formatter: (val) => {
                let loc = findLocation(val);
                return loc.address
              }},
              { name: 'due', title: 'Due', breakpoints: 'xs sm'},
              { name: 'logistic', title: 'Type', breakpoints: 'xs sm'},
              { name: 'destination', title: 'Destination', breakpoints: 'xs sm md lg'},
              { name: 'distance', title: 'Distance', breakpoints: 'xs sm md lg'},
              { name: 'driving_time', title: 'Driving Time', breakpoints: 'xs sm md lg'},
              { name: 'action', title: 'Action', type: 'html'},
              { name: 'items', title: 'Items', type: 'html', breakpoints: 'all'},
            ],
            rows: formatted_orders
          });

        }
      }
    }
  });

});

Template.restaurantViewOrders.helpers({
  r() {
    return Session.get('restaurant.private')
  },
  equals(a, b) {
    return a === b;
  },
});

Template.restaurantViewOrders.events({
  'click .update-cancelled'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('updateOrderCancelled', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Order Cancelled');
      }
    });
  },
  'click .update-confirmed'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('updateOrderConfirmed', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Order Confirmed');
      }
    });
  },
  'click .update-unconfirmed'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('updateOrderUnconfirmed', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Order Un-Confirmed');
      }
    });
  },
  'click .update-ready'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('updateOrderReady', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Order Ready');
      }
    });
  },
  'click .update-on-route'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('updateOrderOnRoute', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Order On-Route');
      }
    });
  },
  'click .update-complete'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('updateOrderComplete', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Order Complete');
      }
    });
  },
  'change #filterLocation'(e) {
    let filtering = FooTable.get('#table').use(FooTable.Filtering), // get the filtering component for the table
      filter = $('#filterLocation').val(); // get the value to filter by
    if (filter === ''){ // if the value is "none" remove the filter
      filtering.removeFilter('location');
      filtering.clear();
    } else { // otherwise add/update the filter.
      filtering.addFilter('location', filter, ['location']);
      filtering.filter();
    }
  },
  'change #filterSearch'(e) {
    let filtering = FooTable.get('#table').use(FooTable.Filtering), // get the filtering component for the table
      filter = $('#filterSearch').val(); // get the value to filter by
    if (filter === ''){ // if the value is "none" remove the filter
      filtering.removeFilter('all');
      filtering.clear();
    } else { // otherwise add/update the filter.
      filtering.addFilter('all', filter, ['name', 'no', 'placed', 'status', 'phone', 'email', 'total', 'payment_method', 'store', 'due', 'type', 'destination', 'distance', 'items']);
      filtering.filter();
    }
  },
});

function findLocation(id) {
  let index = null;
  const r = Restaurant.findOne();
  if (r) {
    r.settings.locations.forEach((p, i) => { if (p._id == id ) index = i; });
    return r.settings.locations[index];
  }
}