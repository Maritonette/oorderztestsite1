import { currencyList, locationTimes, emptyLocationTimes, newLocationTimes } from '../../../../../lib/data_sets.js';
import { S3UploadePromise, S3DeletePromise, GoogleMapsPlaceGetCords } from '../../../../config/async-promises.js';

Template.restaurantSettings.onRendered(function () {

  $(".select2").select2();

  $('.colorpicker-component').colorpicker({
    format: 'rgba'
  });

  // BASIC FORM

  $('#basic-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
  });

  // REGION FORM

  $('#region-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
  });

  // LOGISTIC FORM

  $('#logistic-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
    rules: {
      logistics: {
        required: true
      }
    },
    messages: {
      logistics: "Please select at least 1 order method",
    }
  });

  // PAYMENT FORM

  $('#payment-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
    rules: {
      payments: {
        required: true
      }
    },
    messages: {
      payments: "Please select at least 1 payment method which you will accept"
    }
  });

  // LOCATIONS FORM

  Tracker.autorun(() => {
    // Google maps address fields
    const locations = this.viewData.get('locations');
    if (locations.length && GoogleMaps.loaded()) {
      Meteor.setTimeout(() => {
        locations.forEach((p) => {
          // SETUP GOOGLE MAPS PLACE AUTO COMPLETE
          new google.maps.places.Autocomplete(document.getElementById(`${p._id}-address`));
          $(`#${p._id}-address`).prop('placeholder', '');
        });
        $(".select2").select2();
        $('.clockpicker').clockpicker();
        $('#locations-form').validate({
          errorPlacement: function (error, element) {
            if (element.attr('data-id') == 'time-ctrl' || element.attr('data-id') == 'time-ctr')
              element.parent().before(error);
            else
              element.before(error);
          }
        });
      }, 10);
    }
  });

  // STATUS FORM

  $('#status-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
    rules: {
      autoConfirmTime: {
        digits: true
      }
    },
    messages: {
      autoConfirmTime: {
        digits: 'Please enter a whole number'
      }
    }
  });


  $('#design-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
    rules: {
      primaryColorInput: { rgba: true },
      primaryTextColorInput: { rgba: true },
      mainTextColorInput: { rgba: true },
      menuTextColorInput: { rgba: true },
      backgroundColorInput: { rgba: true },
      boxColorInput: { rgba: true },
      secondaryBoxColorInput: { rgba: true },
      menuColorInput: { rgba: true },
      borderColorInput: { rgba: true }
    }
  });




});

Template.restaurantSettings.onCreated(function () {

  this.viewData = new ReactiveDict();
  const startLocs = newLocationTimes();
  this.viewData.set('locations', []);
  this.viewData.set('logistics', {pickup: false, delivery: false});
  this.viewData.set('statuses', {enable: false, auto: false});
  this.viewData.set('creditCard', false);
  this.viewData.set('stripe', {connected: false, charges_enabled: false});

  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        Session.set('restaurant.private', r);

        const locs = r.settings.locations;
        const viewLocs = [];
        locs.forEach((p, i) => {
          const newTime = {
            _id: p._id,
            days: [
              { _id: 'Monday', times: [] }, { _id: 'Tuesday', times: [] }, { _id: 'Wednesday', times: [] },
              { _id: 'Thursday', times: [] }, { _id: 'Friday', times: [] }, { _id: 'Saturday', times: [] },
              { _id: 'Sunday', times: [] }
            ]
          };
          for (var day in p.opening_times) {
            if (p.opening_times.hasOwnProperty(day)) {
              let ind = null;
              newTime.days.forEach((p, i) => { if (p._id == day) ind = i; });
              if (p.opening_times[day][0].open && p.opening_times[day][0].close) {
                let times = p.opening_times[day].length;
                newTime.days[ind].times = [];
                for (let i = 0; i < times; i++) newTime.days[ind].times.push(i + 1);
              }
              else {
                newTime.days[ind].times = [0];
              }
            }
          }
          viewLocs.push(newTime);
        });
        this.viewData.set('locations', viewLocs);
        this.viewData.set('logistics', {pickup: r.settings.logistic.pickup.enabled , delivery: r.settings.logistic.delivery.enabled});
        this.viewData.set('statuses', {auto:  r.settings.status.auto.enabled});
        this.viewData.set('creditCard', r.settings.payment.credit_card.enabled);
        this.viewData.set('stripe', {connected: r.settings.payment.stripe.connected, charges_enabled: r.settings.payment.stripe.charges_enabled});

        Meteor.setTimeout(() => {
          $('#restaurant-name').val(r.name);
          $('#storeEmail').val(r.settings.info.store_email);
          if (r.settings.info.terms)  $('#termsConditions').val(r.settings.info.terms.replace("\\r\\n", "\r\n"));
          $('#timezone').val(r.settings.region.timezone).trigger("change");
          $('#currency').val(r.settings.region.currency).trigger("change");
          $('#pickup').prop('checked', r.settings.logistic.pickup.enabled);
          $('#pickupNotes').val(r.settings.logistic.pickup.notes);
          $('#delivery').prop('checked', r.settings.logistic.delivery.enabled);
          $('#deliveryNotes').val(r.settings.logistic.delivery.notes);
          $('#deliveryMinOrder').val(r.settings.logistic.delivery.conditions.min_order);
          $('#deliveryMaxDistance').val(r.settings.logistic.delivery.conditions.max_distance);
          $('#deliveryMaxDrivingTime').val(r.settings.logistic.delivery.conditions.max_driving_time);
          $('#paymentCash').prop('checked', r.settings.payment.cash.enabled);
          $('#paymentEfptos').prop('checked', r.settings.payment.efptos.enabled);
          $('#paymentCreditCard').prop('checked', r.settings.payment.credit_card.enabled);
          $('#enableAutoStatus').prop('checked', r.settings.status.auto.enabled);
          $('#autoConfirmTime').val(r.settings.status.auto.time.confirmed);
          $('#autoReadyTime').val(r.settings.status.auto.time.ready);
          $('#autoOnRouteTime').val(r.settings.status.auto.time.on_route);
          $('#autoCompleteTime').val(r.settings.status.auto.time.complete);
          // DESIGN SETTINGS
          $('#primaryColor').colorpicker('setValue', r.settings.design.colors.primary);
          $('#primaryTextColor').colorpicker('setValue', r.settings.design.colors.primary_text);
          $('#mainTextColor').colorpicker('setValue', r.settings.design.colors.main_text);
          $('#menuTextColor').colorpicker('setValue', r.settings.design.colors.menu_text);
          $('#backgroundColor').colorpicker('setValue', r.settings.design.colors.background);
          $('#boxColor').colorpicker('setValue', r.settings.design.colors.box);
          $('#secondaryBoxColor').colorpicker('setValue', r.settings.design.colors.secondary_box);
          $('#menuColor').colorpicker('setValue', r.settings.design.colors.menu);
          $('#borderColor').colorpicker('setValue', r.settings.design.colors.border);
          $('#inputColor').colorpicker('setValue', r.settings.design.colors.input);
          $('#inputTextColor').colorpicker('setValue', r.settings.design.colors.input_text);
          // IMAGES
          $('#existingLogoImage > .col-sm-9').empty();
          $('#existingBackgroundImage > .col-sm-9').empty();
          if (r.settings.design.images.logo.url)
            $('#existingLogoImage > .col-sm-9').append(`<img class="thumbnail-img" src="${r.settings.design.images.logo.url}"/><br>\
          <button type="button" class="btn btn-warning btn-xs m-t-xs deleteLogo">Remove Image</button>`);
          else
            $('#existingLogoImage > .col-sm-9').append(`<p class="m-t-sm">No image</p>`);
          if (r.settings.design.images.background.url)
            $('#existingBackgroundImage > .col-sm-9').append(`<img class="thumbnail-img" src="${r.settings.design.images.background.url}"/>\
          <br><button type="button" class="btn btn-warning btn-xs m-t-xs deleteBackground">Remove Image</button>`);
          else
            $('#existingBackgroundImage > .col-sm-9').append(`<p class="m-t-sm">No image</p>`);
          //STRETCH BACKGROUND
          $('#stretchBackground').prop('checked', r.settings.design.images.background.stretch);
          // LOCATIONS
          viewLocs.forEach((p, i) => {
            $(`#${p._id}-address`).val(locs[i].address);
            p.days.forEach((p1, i1) => {
              p1.times.forEach((p2, i2) => {
                const time = locs[i].opening_times[p1._id][i2];
                $(`#${p._id}-${p1._id}-${p2}-open`).val(time.open);
                $(`#${p._id}-${p1._id}-${p2}-close`).val(time.close);
              });
            });
          });

        }, 15);
      }
    }
  });
});

Template.restaurantSettings.helpers({
  currency() {
    return currencyList;
  },
  timezones() {
    return moment.tz.names();
  },
  locations() {
    return Template.instance().viewData.get('locations');
  },
  creditCard() {
    return Template.instance().viewData.get('creditCard');
  },
  stripe() {
    return Template.instance().viewData.get('stripe');
  },
  logistics() {
    return Template.instance().viewData.get('logistics');
  },
  statuses() {
    return Template.instance().viewData.get('statuses');
  },
  overOne(num) {
    return num > 1
  },
  overZero(n) {
    return n > 0
  },
  addOne(n) {
    return n + 1;
  },
});

Template.restaurantSettings.events({
  'submit #basic-form'(e, t) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#basic-form').valid();
    if (formValid) {
      const name = $('#restaurant-name').val();
      const terms = $('#termsConditions').val();
      const info = {
        store_email:  $('#storeEmail').val() ? $('#storeEmail').val() : null,
        terms: terms ? terms.replace("\r\n", "\\r\\n") : null
      };
      Meteor.call('updateRestaurantBasic', name, info, function (err) {
        if (err) toastr.error('Error', err);
        else toastr.success('Restaurant Updated');
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'submit #locations-form'(e, ins) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#locations-form').valid();
    if (formValid) {
      const r = Session.get('restaurant.private');
      // BUILD THE LOCATIONS
      const vLocations = ins.viewData.get('locations');
      const locations = [];
      vLocations.forEach((p, i) => {
        // Check if ID already exists
        let index = null;
        r.settings.locations.forEach((o, k) => { if (o._id == p._id) { index = k; } });
        let loc = {};
        // EXISTING LOCATION
        if (index || index == 0) {
          loc = {
            _id: r.settings.locations[index]._id,
            created: r.settings.locations[index].created,
            address: $(`#${p._id}-address`).val(),
            open: r.settings.locations[index].open,
            opening_times: {
              Monday: [],
              Tuesday: [],
              Wednesday: [],
              Thursday: [],
              Friday: [],
              Saturday: [],
              Sunday: []
            }
          };
        }
        else {
          // NEW LOCATION
          loc = {
            _id: p._id,
            address: $(`#${p._id}-address`).val(),
            created: Date.now(),
            open: false,
            opening_times: {
              Monday: [],
              Tuesday: [],
              Wednesday: [],
              Thursday: [],
              Friday: [],
              Saturday: [],
              Sunday: []
            }
          };
        }
        p.days.forEach((l, j) => {
          l.times.forEach((m, n) => {
            if (m) {
              const open = $(`#${p._id}-${l._id}-${m}-open`).val();
              const close = $(`#${p._id}-${l._id}-${m}-close`).val();
              loc.opening_times[l._id].push({open: open, close: close});
            }
            else {
              loc.opening_times[l._id].push({open: null, close: null});
            }
          });
        });
        locations.push(loc);
      });

      let promiseArray = [];
      locations.forEach((p, i) => { promiseArray.push(GoogleMapsPlaceGetCords(p.address)) });
      Promise.all(promiseArray).then((res) => {
        res.forEach((p, k) => {
          locations[k].map_data = {
            geometry: {
              lat: p[0].geometry.location.lat(),
              lng: p[0].geometry.location.lng()
            }
          }
        });
        Meteor.call('updateRestaurantLocations', locations, function(err){
          if (err) {
            toastr.error(err.error, 'Error');
          }
          else toastr.success('Restaurant Updated');
          $.unblockUI();
        });
      }).catch((err) => {
        if (err == 'Geocode was not successful for the following reason: ZERO_RESULTS') toastr.error('Please ensure all your locations have proper addresses');
        else toastr.error(err, 'Error');
        $.unblockUI();
      });

    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'submit #region-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#region-form').valid();
    if (formValid) {
      const region = {
        currency: $('#currency').val(),
        timezone: $('#timezone').val()
      };
      Meteor.call('updateRestaurantRegion', region, function (err) {
        if (err) toastr.error('Error', err);
        else toastr.success('Restaurant Updated');
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'submit #logistic-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#logistic-form').valid();
    if (formValid) {
      const logistic = {
        pickup: {
          enabled: $('#pickup').prop('checked'),
          notes: $('#pickupNotes').val() ? $('#pickupNotes').val() : null
        },
        delivery: {
          enabled: $('#delivery').prop('checked'),
          notes: $('#deliveryNotes').val() ? $('#deliveryNotes').val() : null,
          conditions: {
            min_order: $('#deliveryMinOrder').val() ? Math.round($('#deliveryMinOrder').val() * 100) / 100 : null,
            max_distance: $('#deliveryMaxDistance').val() ? Math.round($('#deliveryMaxDistance').val() * 100) / 100 : null,
            max_driving_time: $('#deliveryMaxDrivingTime').val() ? Math.round($('#deliveryMaxDrivingTime').val() * 100) / 100 : null,
          }
        }
      };
      Meteor.call('updateRestaurantLogistic', logistic, function (err) {
        if (err) toastr.error('Error', err);
        else toastr.success('Restaurant Updated');
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'submit #payment-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#payment-form').valid();
    if (formValid) {
      const payment = {
        cash: {
          enabled: $('#paymentCash').prop('checked')
        },
        efptos: {
          enabled: $('#paymentEfptos').prop('checked')
        },
        credit_card: {
          enabled: $('#paymentCreditCard').prop('checked')
        }
      };
      Meteor.call('updateRestaurantPayment', payment, function (err) {
        if (err) toastr.error('Error', err);
        else toastr.success('Restaurant Updated');
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'submit #status-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#status-form').valid();
    if (formValid) {
      let confirmed = $('#autoConfirmTime').val() ? parseInt($('#autoConfirmTime').val()) : null;
      let ready = $('#autoReadyTime').val() ? parseInt($('#autoReadyTime').val()) : null;
      let onRoute = $('#autoOnRouteTime').val() ? parseInt($('#autoOnRouteTime').val()) : null;
      let complete = $('#autoCompleteTime').val() ? parseInt($('#autoCompleteTime').val()) : null;
      const status = {
        auto: {
          enabled: $('#enableAutoStatus').prop('checked'),
          time: {
            confirmed: confirmed,
            ready: ready,
            on_route: onRoute,
            complete: complete
          }
        }
      };
      Meteor.call('updateRestaurantStatus', status, function (err) {
        if (err) toastr.error('Error', err);
        else toastr.success('Restaurant Updated');
        $.unblockUI();
      });
    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'submit #design-form'(e) {
    e.preventDefault();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const formValid = $('#design-form').valid();
    if (formValid) {

      const r = Session.get('restaurant.private');
      const design = {
        colors: {
          primary: $('#primaryColorInput').val() ? $('#primaryColorInput').val(): null,
          primary_text: $('#primaryTextColorInput').val() ? $('#primaryTextColorInput').val() : null,
          main_text: $('#mainTextColorInput').val() ? $('#mainTextColorInput').val() : null,
          menu_text: $('#menuTextColorInput').val() ? $('#menuTextColorInput').val() : null,
          background: $('#backgroundColorInput').val() ? $('#backgroundColorInput').val() : null,
          box: $('#boxColorInput').val() ? $('#boxColorInput').val() : null,
          secondary_box: $('#secondaryBoxColorInput').val() ? $('#secondaryBoxColorInput').val() : null,
          menu: $('#menuColorInput').val() ? $('#menuColorInput').val() : null,
          border: $('#borderColorInput').val() ? $('#borderColorInput').val() : null,
          input: $('#inputColorInput').val() ? $('#inputColorInput').val() : null,
          input_text: $('#inputTextColorInput').val() ? $('#inputTextColorInput').val() : null,
        },
        images: {
          logo: {
            url: r.settings.design.images.logo.url,
            rel: r.settings.design.images.logo.rel
          },
          background: {
            url: r.settings.design.images.background.url,
            rel: r.settings.design.images.background.rel,
            stretch: $('#stretchBackground').prop('checked')
          }
        }
      };

      // IMAGE UPLOAD PROMISE
      const logoImage = $('input.logo-image')[0].files;
      const backgroundImage = $('input.background-image')[0].files;
      const logoPromise = S3UploadePromise(logoImage, 250000, `images/${r.id}/store`);
      const backgroundPromise = S3UploadePromise(backgroundImage, 400000,`images/${r.id}/store`);
      // LOGO & BACKGROUND DELETE
      let deleteLogoPromise = S3DeletePromise(null);
      let deleteBackgroundPromise = S3DeletePromise(null);
      if (logoImage.length > 0)
        deleteLogoPromise = S3DeletePromise(r.settings.design.images.logo.rel);
      if (backgroundImage.length > 0)
        deleteBackgroundPromise = S3DeletePromise(r.settings.design.images.background.rel);

      Promise.all([logoPromise, backgroundPromise, deleteLogoPromise, deleteBackgroundPromise]).then((vals) => {
        // 0 - STATIC RESTAURANT DATA - 1 - LOGO - 2 - BACKGROUND
        if (vals[0] !== 'No File') {
          design.images.logo.url = vals[0].secure_url;
          design.images.logo.rel = vals[0].relative_url;
        }
        if (vals[1] !== 'No File') {
          design.images.background.url = vals[1].secure_url;
          design.images.background.rel = vals[1].relative_url;
        }
        Meteor.call('updateRestaurantDesign', design, function (err) {
          if (err) toastr.error('Error', err);
          else {
            $('#backgroundImage').val('');
            $('#logoImage').val('');
            toastr.success('Restaurant Updated');
          }
          $.unblockUI();
        });
      }).catch((err) => {
        toastr.error(err, 'Error');
        $.unblockUI();
      });

    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .deleteLogo'() {
    $.blockUI({ message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const r = Session.get('restaurant.private');
    const deleteImage = S3DeletePromise(r.settings.design.images.logo.rel);
    deleteImage.then(() => {
      Meteor.call('deleteLogoImage', (err) => {
        if (err) toastr.error(err, 'Error');
        else toastr.success('Logo image removed');
        $.unblockUI();
      });
    });
  },
  'click .deleteBackground'() {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const r = Session.get('restaurant.private');
    const deleteImage = S3DeletePromise(r.settings.design.images.background.rel);
    deleteImage.then(() => {
      Meteor.call('deleteBackgroundImage', (err) => {
        if (err) toastr.error(err, 'Error');
        else toastr.success('Background image removed');
        $.unblockUI();
      });
    });
  },
  'click .close-day'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    locations[locationIndex].days[dayIndex].times = [0];
    ins.viewData.set('locations', locations);
  },
  'click .open-day'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    locations[locationIndex].days[dayIndex].times = [1];
    ins.viewData.set('locations', locations);
  },
  'click .add-time'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    console.log(ids);
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    const l = locations[locationIndex].days[dayIndex].times.length;
    locations[locationIndex].days[dayIndex].times.push(l + 1);
    ins.viewData.set('locations', locations);
  },
  'click .remove-time'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    const l = locations[locationIndex].days[dayIndex].times.length;
    locations[locationIndex].days[dayIndex].times.splice(l - 1, 1);
    ins.viewData.set('locations', locations);
  },
  'click .add-location'(e, ins) {
    const locations = ins.viewData.get('locations');
    const newTime = newLocationTimes();
    locations.push(newTime);
    ins.viewData.set('locations', locations);
  },
  'click .remove-location'(e, ins) {
    const id = $(e.currentTarget).attr('data-id');
    const locations = ins.viewData.get('locations');
    let index = null;
    locations.forEach((p, i) => { if (p._id == id) index = i; });
    locations.splice(index, 1);
    ins.viewData.set('locations', locations);
  },
  'change #pickup, change #delivery'(e, ins) {
    ins.viewData.set('logistics', {
      pickup: $('#pickup').prop('checked'),
      delivery: $('#delivery').prop('checked')
    });
  },
  'change #paymentCreditCard'(e, ins) {
    ins.viewData.set('creditCard', $('#paymentCreditCard').prop('checked'));
  },
  'click .disconnect-stripe'() {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    Meteor.call('disconnectStripe', function(err){
      if (err) toastr.error(err);
      else toastr.success('Stripe Disconnected');
      $.unblockUI();
    });
  },
  'click .validate-stripe'() {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    Meteor.call('validateStripe', function(err){
      if (err) toastr.error(err);
      else toastr.success('Stripe Updated');
      $.unblockUI();
    });
  },
  'change #enableAutoStatus'(e, ins) {
    ins.viewData.set('statuses', {
      auto: $('#enableAutoStatus').prop('checked')
    });
  },
});
