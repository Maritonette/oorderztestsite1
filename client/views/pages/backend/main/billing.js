Template.restaurantBilling.onRendered(function () {

});

Template.restaurantBilling.onCreated(function () {

  this.viewData = new ReactiveDict();
  this.viewData.set('est.subscription.cost', 39);
  this.viewData.set('account.status', null);
  let template = Template.instance();

  this.createInvoiceTable = function(data, r) {
    $('#table').remove();
    $('#table-container').append(`<table id="table" class="table" data-sorting="true"></table>`);
    $('#table').footable({
      "filtering": {
        "enabled": true
      },
      "paging": {
        "enabled": true
      },
      columns: [
        { name: 'date', title: 'Date', formatter(val) {
          return moment.utc(val * 1000).tz(r.settings.region.timezone) .format('DD/MM/YY');
        }},
        { name: 'total', title: 'Total' , formatter(val) {
          return `$${val / 100}`
        }},
        { name: 'paid', title: 'Paid' , formatter(val) {
          return val ? 'Yes' : 'No'
        }},
        { name: 'lines.data[0].quantity', title: 'Description', formatter(val) {
          return 'Oorderz Subscription'
        } },
      ],
      rows: data
    });
  };

  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {

        Session.set('restaurant.private', r);

        // SUBSCRIPTION COST IF ANY
        $('#numberOfLocationsSubscribed').val(r.billing.locations);
        template.viewData.set('subscription.cost', (r.billing.locations * 39));
        
        // ACCOUNT STATUS
        const now = moment.utc(Date.now());
        let account_status = null;
        if (r.billing.status == 1) {
          const trial_ends = moment.utc(r.created + (30 * 24 * 60 * 60 * 1000));
          const difference = trial_ends.fromNow();
          if (r.billing.disable) { // DISABLED TRIAL ACCOUNT
            account_status = `Trial ended ${difference}. Subscribe now to continue use`
          }
          else { // NOT DISABLED TRIAL ACCOUNT
            account_status = `Trial ends ${difference}. Subscribe now and you will only be charged at the end of your trial`
          }
        }
        else if (r.billing.status == 2) {
          account_status = `Currently subscribed with ${r.billing.locations} locations`
        }
        else if (r.billing.status == 3) {
          const cancel_at = moment.utc(r.billing.cancel_at);
          const difference = cancel_at.fromNow();
          let reason = ``;
          if (!r.billing.source) reason = ` because your payment failed`;
          else reason = ``;
          if (r.billing.disable) { // DISABLED TRIAL ACCOUNT
            account_status = `Subscription cancelled ${difference}${reason}. Account will be disabled and stores will no longer display, subscribe now to continue use`
          }
          else { // NOT DISABLED TRIAL ACCOUNT
            account_status = `Subscription cancelled${reason}. Account will be disabled ${difference} after which your stores will no longer display. Subscribe anytime now or after to continue use`
          }
        }
        template.viewData.set('account.status', account_status);

        // GET INVOICES
        Meteor.call('stripeGetInvoices', (err, res) => {
          if (err) toastr.error(err.message, 'Error Getting Invoices');
          else {
            template.createInvoiceTable(res.data, r)
          }
        });
      }
    }
  });


});

Template.restaurantBilling.helpers({
  r() {
    return Session.get('restaurant.private');
  },
  accountStatus() {
    return Template.instance().viewData.get('account.status');
  },
  equals(a, b) {
    return a == b
  },
  orEquals(a, b, c) {
    return a == b || a == c
  },
  estSubscriptionCost() {
    const cost = Template.instance().viewData.get('est.subscription.cost');
    return cost ? cost : '?';
  },
  subscriptionCost() {
    const cost = Template.instance().viewData.get('subscription.cost');
    return cost ? cost : '?';
  }
});

Template.restaurantBilling.events({
  'change #numberOfLocations'(e, ins) {
    ins.viewData.set('est.subscription.cost', (parseInt($('#numberOfLocations').val()) * 39));
  },
  'change #numberOfLocationsSubscribed'(e, ins) {
    ins.viewData.set('subscription.cost', (parseInt($('#numberOfLocationsSubscribed').val()) * 39));
  },
  'click .stripeAddCard'(e, ins) {
    e.preventDefault();
    const u = Meteor.user();
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    Meteor.setTimeout(() => { $.unblockUI() }, 1000);
    StripeCheckout.open({
      //image: r.settings.design.images.logo.url,
      locale: 'auto',
      key: Meteor.settings.public.stripe,
      name: 'Oorderz Platform',
      description: `You won't be charged till you subscribe`,
      currency: 'usd',
      panelLabel: 'Add Card',
      zipCode: true,
      email: u.emails[0].address,
      allowRememberMe: false,
      token(token) {
        $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
        Meteor.call('stripeUpdateCard', token, (err, res) => {
          if (err) {
            toastr.error(err.message, 'Error');
            console.log(err);
          }
          else toastr.success('Your account has been updated', 'Success');
          $.unblockUI();
        });
      }
    });
  },
  'click .create-subscription'(e, ins) {
    e.preventDefault();
    $('#locationHelpBlock').html('');
    const r = Session.get('restaurant.private');
    const numberOfLocations = $('#numberOfLocations').val() ? parseInt($('#numberOfLocations').val()) : null;
    let error = null;
    if (r.settings.locations.length > numberOfLocations) error = "You currently have more locations than you are subscribing for. Please subscribe for more locations or remove some.";
    else if (!Number.isInteger(numberOfLocations)) error = "Invalid number of locations";
    else if (!r.billing.source) error = 'You do not have an active payment method, please add one first';
    if (!error) {
      swal({
        title: "Subscribe",
        showCancelButton: true,
        confirmButtonColor: "#f8994b",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
      }, function() {
        $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
        Meteor.call('stripeCreateSubscription', numberOfLocations, (err, res) => {
          if (err) toastr.error(err.message, 'Error');
          else toastr.success('Your subscription has been confirmed');
          $.unblockUI();
        });
      });
    }
    else {
      $('#locationHelpBlock').html(error);
    }
  },
  'click .update-subscription'(e, ins) {
    e.preventDefault();
    $('#locationHelpBlockSubscribed').html('');
    const r = Session.get('restaurant.private');
    const numberOfLocations = $('#numberOfLocationsSubscribed').val() ? parseInt($('#numberOfLocationsSubscribed').val()) : null;
    let error = null;
    if (r.settings.locations.length > numberOfLocations) error = "You currently have more locations than you are subscribing for. Please subscribe for more locations or remove some.";
    else if (!Number.isInteger(numberOfLocations)) error = "Invalid number of locations";
    else if (!r.billing.source) error = 'You do not have an active payment method, please add one first';
    else if (r.billing.locations == numberOfLocations) error = 'Already subscribed to the chosen amount of locations';
    if (!error) {
      swal({
        title: "Update Subscription",
        showCancelButton: true,
        confirmButtonColor: "#f8994b",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
      }, function() {
        $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
        Meteor.call('stripeUpdateSubscription', numberOfLocations, (err, res) => {
          if (err) toastr.error(err.message, 'Error');
          else toastr.success('Your subscription has been updated');
          $.unblockUI();
        });
      });
    }
    else {
      $('#locationHelpBlockSubscribed').html(error);
    }
  },
  'click .cancel-subscription'(e, ins) {
    e.preventDefault();
    swal({
      title: "Cancel Subscription",
      text: "Sorry to hear you are leaving us. If there is something we could do better please let us know. Your account will remain active until the end of your subscription period.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#ed5565",
      confirmButtonText: "Cancel",
      closeOnConfirm: true,
    }, function() {
      $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
      Meteor.call('stripeCancelSubscription', (err, res) => {
        if (err) toastr.error(err.message, 'Error');
        else toastr.success('Your subscription has been cancelled');
        $.unblockUI();
      });
    });
  },
  'click .get-stripe-invoices'(e, ins) {
    $.blockUI({message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'});
    const r = Session.get('restaurant.private');
    Meteor.call('stripeGetInvoices', (err, res) => {
      if (err) toastr.error(err.message, 'Error Getting Invoices');
      else {
        toastr.success('Invoices retrieved');
        ins.createInvoiceTable(res.data, r)
      }
      $.unblockUI();
    });
  },
});
