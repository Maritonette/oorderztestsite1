import { currencyList, locationTimes, newLocationTimes } from '../../../../../lib/data_sets.js';
import { S3UploadePromise, GoogleMapsPlaceGetCords } from '../../../../config/async-promises.js';
// import { Restaurant } from '../../../../lib/restaurant';

Template.restaurantSetup.onRendered(function () {

  // Toggle special class
  $("body").toggleClass("mini-navbar");

  if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
    // Hide menu in order to smoothly turn on when maximize menu
    $('#side-menu').hide();
    // For smoothly turn on menu
    setTimeout(
      function () {
        $('#side-menu').fadeIn(400);
      }, 200);
  } else if ($('body').hasClass('fixed-sidebar')) {
    $('#side-menu').hide();
    setTimeout(
      function () {
        $('#side-menu').fadeIn(400);
      }, 100);
  } else {
    // Remove all inline style from jquery fadeIn function to reset menu state
    $('#side-menu').removeAttr('style');
  }

  $(".select2").select2();

  $('#restaurant-basic-form').validate({
    errorPlacement: function (error, element) {
      if (element.attr('type') == 'checkbox')
        element.parent().before(error);
      else
        element.before(error);
    },
    rules: {
      logistics: {
        required: true
      },
      payments: {
        required: true
      }
    },
    messages: {
      logistics: "Please select at least 1 order method",
      payments: "Please select at least 1 payment method which you will accept"
    }
  });

});

Template.restaurantSetup.onCreated(function () {

  this.viewData = new ReactiveDict();
  const startLocs = newLocationTimes();
  this.viewData.set('locations', [startLocs]);

// Create Subscriptions
  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const restaurant = Restaurant.findOne();
      if (restaurant) {
        Router.go('/dashboard');
      }
    }
  });

  Tracker.autorun(() => {
    // Google maps address fields
    const locations = this.viewData.get('locations');
    if (GoogleMaps.loaded()) {
      locations.forEach((p) => {
        Meteor.setTimeout(() => {
          new google.maps.places.Autocomplete(document.getElementById(`${p._id}-address`));
          $(`#${p._id}-address`).prop('placeholder', '');
          $(".select2").select2();
          $('.clockpicker').clockpicker();
          $('.restaurant-locations-form').validate({
            errorPlacement: function (error, element) {
              if (element.attr('data-id') == 'time-ctrl' || element.attr('data-id') == 'time-ctr')
                element.parent().before(error);
              else
                element.before(error);
            }
          });
        }, 15);
      });
    }
  });

});

Template.restaurantSetup.helpers({
  currency() {
    return currencyList;
  },
  timezones() {
    return moment.tz.names();
  },
  locations() {
    return Template.instance().viewData.get('locations');
  },
  overOne(num) {
    return num > 1
  },
  overZero(n) {
    return n > 0
  },
  addOne(n) {
    return n + 1;
  }
});

Template.restaurantSetup.events({
  'click .create-restaurant-btn'(e, ins) {
    $.blockUI( {message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>'} );
    // CHECK ALL FORMS VALID
    $('#restaurant-basic-form').valid();
    $('.restaurant-locations-form').valid();
    // IF VALID
    if ( $('#restaurant-basic-form').valid() && $('.restaurant-locations-form').valid()) {

      const r = {};
      r.name = $('#restaurant-name').val();
      r.settings = {
        info: {
          store_email: null,
          terms: null
        },
        region: {},
        logistic: {
          pickup: {},
          delivery: {}
        },
        payment: {
          cash: {},
          efptos: {},
        }
      };
      r.settings.region.currency = $('#currency').val();
      r.settings.region.timezone = $('#timezone').val();
      r.settings.logistic.pickup.enabled = $('#pickup').prop('checked');
      r.settings.logistic.delivery.enabled = $('#delivery').prop('checked');
      r.settings.payment.cash.enabled = $('#paymentCash').prop('checked');
      r.settings.payment.efptos.enabled = $('#paymentEfptos').prop('checked');
      // LOCATIONS
      const vLocations = ins.viewData.get('locations');
      const locations = [];
      vLocations.forEach((p, i) => {
        let loc = {};
          // NEW LOCATION
          loc = {
            _id: p._id,
            address: $(`#${p._id}-address`).val(),
            open: false,
            opening_times: {
              Monday: [],
              Tuesday: [],
              Wednesday: [],
              Thursday: [],
              Friday: [],
              Saturday: [],
              Sunday: []
            }
          };
        p.days.forEach((l, j) => {
          l.times.forEach((m, n) => {
            if (m) {
              const open = $(`#${p._id}-${l._id}-${m}-open`).val();
              const close = $(`#${p._id}-${l._id}-${m}-close`).val();
              loc.opening_times[l._id].push({open: open, close: close});
            }
            else {
              loc.opening_times[l._id].push({open: null, close: null});
            }
          });
        });
        locations.push(loc);
      });

      let promiseArray = [];

      locations.forEach((p, i) => { promiseArray.push(GoogleMapsPlaceGetCords(p.address)) });

      Promise.all(promiseArray).then((res) => {

        res.forEach((p, k) => {
          locations[k].map_data = {
            geometry: {
              lat: p[0].geometry.location.lat(),
              lng: p[0].geometry.location.lng()
            }
          }
        });

        r.settings.locations = locations;
        Meteor.call('createRestaurant', r, (err) => {
          if (err) toastr.error(err, 'Form Error');
          else Router.go('/dashboard');
          $.unblockUI();
        });

      }).catch((err) => {
        if (err == 'Geocode was not successful for the following reason: ZERO_RESULTS') toastr.error('Please ensure all your locations have proper addresses');
        else toastr.error(err, 'Error');
        $.unblockUI();
      });

    }
    else {
      $.unblockUI();
      toastr.error('Please correct the marked fields before continuing', 'Invalid Fields')
    }
  },
  'click .close-day'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    locations[locationIndex].days[dayIndex].times = [0];
    ins.viewData.set('locations', locations);
  },
  'click .open-day'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    locations[locationIndex].days[dayIndex].times = [1];
    ins.viewData.set('locations', locations);
  },
  'click .add-time'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    console.log(ids);
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    const l = locations[locationIndex].days[dayIndex].times.length;
    locations[locationIndex].days[dayIndex].times.push(l + 1);
    ins.viewData.set('locations', locations);
  },
  'click .remove-time'(e, ins) {
    const ids = $(e.target).attr('data-btn');
    const idarray = ids.split('-');
    const location = idarray[0];
    const day = idarray[1];
    const locations = ins.viewData.get('locations');
    let locationIndex = null;
    let dayIndex = null;
    locations.forEach((p, i) => {
      if (p._id == location) locationIndex = i;
    });
    locations[locationIndex].days.forEach((p, i) => {
      if (p._id == day) dayIndex = i;
    });
    const l = locations[locationIndex].days[dayIndex].times.length;
    locations[locationIndex].days[dayIndex].times.splice(l - 1, 1);
    ins.viewData.set('locations', locations);
  },
  'click .add-location'(e, ins) {
    const locations = ins.viewData.get('locations');
    const newTime = newLocationTimes();
    locations.push(newTime);
    ins.viewData.set('locations', locations);
  },
  'click .remove-location'(e, ins) {
    const id = $(e.currentTarget).attr('data-id');
    const locations = ins.viewData.get('locations');
    let index = null;
    locations.forEach((p, i) => { if (p._id == id) index = i; });
    locations.splice(index, 1);
    ins.viewData.set('locations', locations);
  },
});
