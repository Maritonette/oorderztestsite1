Template.dashboard.onRendered(function () {

});

Template.dashboard.onCreated(function () {
  
  const userHandle = this.subscribe('user.private');
  
  Tracker.autorun(() => {
    const userReady = userHandle.ready();
    if (userReady) {
      const u = Meteor.user();
      Session.set('user.private', u);
    }
  });

  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        Session.set('restaurant.private', r);
        Session.set('c.icon', r.settings.region.currency_symbol);
      }
    }
  });

  // Create Subscriptions
  const o_handle = this.subscribe('orders.private');
  Tracker.autorun(() => {
    const ready = o_handle.ready();
    const r = Session.get('restaurant.private');
    if (ready) {
      const orders = Order.find().fetch();
      if (orders && r) {
        Session.set('orders.private', orders);

        // Create overall stats
        const stats = {
          totalOrders: orders.length,
          totalSales: 0,
          totalOrdersToday: 0,
          totalSalesToday: null,
        };
        orders.forEach((o) => { stats.totalSales += o.total; });
        stats.totalSales = Math.round(stats.totalSales * 100) / 100;

        // TODAYS DATA, FILTER OUT ORDERS NOT IN TODA

        const today = moment.utc(Date.now()).tz(r.settings.region.timezone);
        let todaysOrders = _.filter(orders, (order) => {
          let day = moment.utc(order.created).tz(r.settings.region.timezone);
          return today.isSame(day, 'd');
        });
        todaysOrders.forEach((o) => {
          stats.totalSalesToday += o.total;
          stats.totalOrdersToday += 1;
        });
        stats.totalSalesToday = Math.round(stats.totalSalesToday * 100) / 100;

        Session.set('stats.private', stats);

        $('#location-table-body').empty();

        const cIcon = r.settings.region.currency_symbol;

        r.settings.locations.forEach((p, i) => {

          let status = p.open ? `<span class="label label-primary">Open</span>`:`<span class="label label-danger">Closed</span>`;

          let actionHtml = `<div class="btn-group dropup">\
                      <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></button>\
                      <ul class="dropdown-menu dropdown-menu-right">`;
          if (p.open) {
            actionHtml += `<li><a data-id="${p._id}" class="close-location">Close</a></li>`;
          }
          if (!p.open) {
            actionHtml += `<li><a data-id="${p._id}" class="open-location">Open</a></li>`;
          }
          actionHtml += `</ul></div>`;

          // GET ORDERS FOR THIS LOCATION
          const locOrders = [];
          orders.forEach((o) => { if (p._id == o.config.location) locOrders.push(o); });

          const stats = {
            unconfirmed: 0,
            total: 0,
            totalSales: 0,
          };

          locOrders.forEach((o) => {
            if (o.status == "Un-Confirmed") stats.unconfirmed += 1;
            stats.total += 1;
            stats.totalSales += o.total;
          });

          stats.totalSales = Math.round(stats.totalSales * 100) / 100;

          $('#location-table-body').append(`
          <tr id="location-${p._id}">\
            <td>${p.address}</td>\
            <td>${status}</td>\
            <td>${stats.unconfirmed}</td>\
            <td>${stats.total}</td>\
            <td>${cIcon}${stats.totalSales}</td>\
            <td>${actionHtml}</td>\
          </tr>`
          );

        });
        $('#location-table').footable({
          toggleColumn: "first"
        });
        
      }
    }
  });

});

Template.dashboard.helpers({
  user() {
    return Session.get('user.private');
  },
  stats() {
    return Session.get('stats.private');
  },
  cIcon() {
    return Session.get('c.icon');
  }
});

Template.dashboard.events({
  'click .open-location'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('restaurantOpenLocation', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Location Opened');
      }
    });
  },
  'click .close-location'(e) {
    const id = $(e.currentTarget).attr('data-id');
    Meteor.call('restaurantCloseLocation', id, (err) => {
      if (err) toastr.error(err, 'Error');
      else {
        toastr.success('Location Closed');
      }
    });
  },
});
