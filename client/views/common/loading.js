Template.loading.rendered = function(){

};

Template.loading.destroyed = function(){

  // $.unblockUI();

};


Template.loading.onCreated(function OnCreated() {

  $.blockUI({
    message : '<div class="sk-spinner sk-spinner-double-bounce"> <div class="sk-double-bounce1"></div> <div class="sk-double-bounce2"></div> </div>',
    overlayCSS: {
      opacity: 1
    },
    fadeIn:  0
  });

});

Template.loading.helpers({

});

Template.loading.events({


});