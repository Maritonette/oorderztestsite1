Template.navigation.rendered = function(){
    // Initialize metisMenu
    $('#side-menu').metisMenu();
};

Template.navigation.onCreated(function navOnCreated() {
// Create Subscriptions
    const handle = this.subscribe('restaurant.private');
    Tracker.autorun(() => {
        const restaurantsReady = handle.ready();
        if (restaurantsReady) {
            const r = Restaurant.findOne();
            if (r) {
                Session.set('restaurant.private', r);
                // SPLIT STORE NAME
                Meteor.setTimeout(() => {
                    $('#side-menu').metisMenu();
                }, 10);
            }
        }
    });
});

Template.navigation.helpers({
    r() {
        return Restaurant.findOne();
    },
    splitName() {
      const r = Session.get('restaurant.private');
      if (r) {
        const splitName = r.name.split(" ");
        let splitNameDisplay = "";
        splitName.forEach((p) => { splitNameDisplay = splitNameDisplay + p[0]; });
        return splitNameDisplay;
      }
    }
});


Template.navigation.events({
    'click .logout-user'() {
        Meteor.logout();
        Router.go('/login');
    },
});
