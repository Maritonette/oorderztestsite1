Template.topNavbar.rendered = function(){

    // FIXED TOP NAVBAR OPTION
    // Uncomment this if you want to have fixed top navbar
    // $('body').addClass('fixed-nav');
    // $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');

};

Template.topNavbar.onCreated(function(){

  this.viewData = new ReactiveDict();
  this.viewData.set('status', 'red');
  this.viewData.set('account.status', null);
  const template = Template.instance();


  Meteor.autorun(() => {
    let stat;
    if (Meteor.status().status === "connected") stat = 'green';
    else if (Meteor.status().status === "connecting") stat = 'yellow';
    else stat = 'red';
    template.viewData.set('status', stat);
  });

  const handle = this.subscribe('restaurant.private');
  Tracker.autorun(() => {
    const restaurantsReady = handle.ready();
    if (restaurantsReady) {
      const r = Restaurant.findOne();
      if (r) {
        let status = null;
        if (r.billing.status == 1) {
          if (r.billing.disable) status = `Trial Expired`;
          else {
            const trial_ends = moment.utc(r.created + (30 * 24 * 60 * 60 * 1000));
            const difference = trial_ends.fromNow();
            status = `Trial ends ${difference}`
          }
        }
        else if (r.billing.status == 2) status = `Subscribed`;
        else if (r.billing.status == 3) status = `Cancelled`;
        else status = 'Error';
        template.viewData.set('account.status', status);
      }

    }
  });

});


Template.topNavbar.helpers({
    accountStatus() {
        return Template.instance().viewData.get('account.status');
    },
    status() {
        return Template.instance().viewData.get('status');
    },
    equals(a, b) {
        return a === b;
    },
});

Template.topNavbar.events({

    'click .logout-user'() {
        Meteor.logout();
        Router.go('/login');
    },

    // Toggle left navigation
    'click #navbar-minimalize': function(event){

        event.preventDefault();

        // Toggle special class
        $("body").toggleClass("mini-navbar");

        // Enable smoothly hide/show menu
        if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
            // Hide menu in order to smoothly turn on when maximize menu
            $('#side-menu').hide();
            // For smoothly turn on menu
            setTimeout(
                function () {
                    $('#side-menu').fadeIn(400);
                }, 200);
        } else if ($('body').hasClass('fixed-sidebar')) {
            $('#side-menu').hide();
            setTimeout(
                function () {
                    $('#side-menu').fadeIn(400);
                }, 100);
        } else {
            // Remove all inline style from jquery fadeIn function to reset menu state
            $('#side-menu').removeAttr('style');
        }
    }

});
