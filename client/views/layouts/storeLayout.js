Template.storeLayout.rendered = function(){

  $('iframe').remove();

  /*
  // Add special class for handel top navigation layout
  $('body').addClass('top-navigation');
  // FIX TOP NAV
  $('body').addClass('fixed-nav');
  $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
  */

  // Minimalize menu when screen is less than 768px
  $(window).bind("resize load", function () {
    if ($(this).width() < 769) {
      $('body').addClass('body-small')
    } else {
      $('body').removeClass('body-small')
    }
  });

  // Fix height of layout when resize, scroll and load
  $(window).bind("load resize scroll", function() {
    if(!$("body").hasClass('body-small')) {

      var navbarHeigh = $('nav.navbar-default').height();
      var wrapperHeigh = $('#page-wrapper').height();

      if(navbarHeigh > wrapperHeigh){
        $('#page-wrapper').css("min-height", navbarHeigh + "px");
      }

      if(navbarHeigh < wrapperHeigh){
        $('#page-wrapper').css("min-height", $(window).height()  + "px");
      }

      if ($('body').hasClass('fixed-nav')) {
        if (navbarHeigh > wrapperHeigh) {
          $('#page-wrapper').css("min-height", navbarHeigh - 60 + "px");
        } else {
          $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
        }
      }
    }
  });

};

Template.storeLayout.destroyed = function(){

  // Remove special top navigation class
  /*$('body').removeClass('top-navigation');*/
};


Template.storeLayout.onCreated(function OnCreated() {

  // NORMAL SUBSCRIPTION + STORE VALIDATION

  const rid = Router.current().params.id;
  const handle = this.subscribe('restaurant.public', rid);
  Tracker.autorun(() => {
    const ready = handle.ready();
    if (ready) {
      const r = Restaurant.findOne();
      Session.set('restaurant.public', r);

      $('title').html(`${r.name} - Order Online`);
      $('#metadescription').attr('content', `Order online now at ${r.name}`);
    }
  });

  Template.instance().shadeBlendConvertor = function(p, from, to) {
    if(typeof(p)!="number"||p<-1||p>1||typeof(from)!="string"||(from[0]!='r'&&from[0]!='#')||(typeof(to)!="string"&&typeof(to)!="undefined"))return null; //ErrorCheck
    if(!this.sbcRip)this.sbcRip=function(d){
      var l=d.length,RGB=new Object();
      if(l>9){
        d=d.split(",");
        if(d.length<3||d.length>4)return null;//ErrorCheck
        RGB[0]=i(d[0].slice(4)),RGB[1]=i(d[1]),RGB[2]=i(d[2]),RGB[3]=d[3]?parseFloat(d[3]):-1;
      }else{
        if(l==8||l==6||l<4)return null; //ErrorCheck
        if(l<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(l>4?d[4]+""+d[4]:""); //3 digit
        d=i(d.slice(1),16),RGB[0]=d>>16&255,RGB[1]=d>>8&255,RGB[2]=d&255,RGB[3]=l==9||l==5?r(((d>>24&255)/255)*10000)/10000:-1;
      }
      return RGB;}
    var i=parseInt,r=Math.round,h=from.length>9,h=typeof(to)=="string"?to.length>9?true:to=="c"?!h:false:h,b=p<0,p=b?p*-1:p,to=to&&to!="c"?to:b?"#000000":"#FFFFFF",f=sbcRip(from),t=sbcRip(to);
    if(!f||!t)return null; //ErrorCheck
    if(h)return "rgb("+r((t[0]-f[0])*p+f[0])+","+r((t[1]-f[1])*p+f[1])+","+r((t[2]-f[2])*p+f[2])+(f[3]<0&&t[3]<0?")":","+(f[3]>-1&&t[3]>-1?r(((t[3]-f[3])*p+f[3])*10000)/10000:t[3]<0?f[3]:t[3])+")");
    else return "#"+(0x100000000+(f[3]>-1&&t[3]>-1?r(((t[3]-f[3])*p+f[3])*255):t[3]>-1?r(t[3]*255):f[3]>-1?r(f[3]*255):255)*0x1000000+r((t[0]-f[0])*p+f[0])*0x10000+r((t[1]-f[1])*p+f[1])*0x100+r((t[2]-f[2])*p+f[2])).toString(16).slice(f[3]>-1||t[3]>-1?1:3);
  }

});

Template.storeLayout.helpers({
  d(){
    const r = Session.get('restaurant.public');
    if (r) return r.settings.design;
  },
  p() {
    const r = Session.get('restaurant.public');
    if (r)
    return {
      // Standard
      primary: r.settings.design.colors.primary,
      primary_text: r.settings.design.colors.primary_text,
      background: r.settings.design.colors.background,
      box: r.settings.design.colors.box,
      secondary_box: r.settings.design.colors.secondary_box,
      main_text: r.settings.design.colors.main_text,
      menu: r.settings.design.colors.menu,
      menu_text: r.settings.design.colors.menu_text,
      input: r.settings.design.colors.input,
      input_text: r.settings.design.colors.input_text,
      border: r.settings.design.colors.border,
      // MIXED
      menu_light: shadeBlendConvert(0.1, r.settings.design.colors.menu),
      menu_hover: shadeBlendConvert(0.2, r.settings.design.colors.menu),
      menu_text_light: shadeBlendConvert(0.1, r.settings.design.colors.menu_text),
    }
  }
});

Template.storeLayout.events({
  // Toggle left navigation
  'click .navbar-minimalize': function(event){

    event.preventDefault();

    // Toggle special class
    $("body").toggleClass("mini-navbar");

    // Enable smoothly hide/show menu
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
      // Hide menu in order to smoothly turn on when maximize menu
      $('#side-menu').hide();
      // For smoothly turn on menu
      setTimeout(
        function () {
          $('#side-menu').fadeIn(400);
        }, 200);
    } else if ($('body').hasClass('fixed-sidebar')) {
      $('#side-menu').hide();
      setTimeout(
        function () {
          $('#side-menu').fadeIn(400);
        }, 100);
    } else {
      // Remove all inline style from jquery fadeIn function to reset menu state
      $('#side-menu').removeAttr('style');
    }
    Meteor.setTimeout(() => { $(window).resize(); }, 10);

  }
});

function shadeBlendConvert(p, from, to) {
  if(typeof(p)!="number"||p<-1||p>1||typeof(from)!="string"||(from[0]!='r'&&from[0]!='#')||(typeof(to)!="string"&&typeof(to)!="undefined"))return null; //ErrorCheck
  if(!this.sbcRip)this.sbcRip=function(d){
    var l=d.length,RGB=new Object();
    if(l>9){
      d=d.split(",");
      if(d.length<3||d.length>4)return null;//ErrorCheck
      RGB[0]=i(d[0].slice(5)),RGB[1]=i(d[1]),RGB[2]=i(d[2]),RGB[3]=d[3]?parseFloat(d[3]):-1;
    }else{
      if(l==8||l==6||l<4)return null; //ErrorCheck
      if(l<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(l>4?d[4]+""+d[4]:""); //3 digit
      d=i(d.slice(1),16),RGB[0]=d>>16&255,RGB[1]=d>>8&255,RGB[2]=d&255,RGB[3]=l==9||l==5?r(((d>>24&255)/255)*10000)/10000:-1;
    }
    return RGB;}
  var i=parseInt,r=Math.round,h=from.length>9,h=typeof(to)=="string"?to.length>9?true:to=="c"?!h:false:h,b=p<0,p=b?p*-1:p,to=to&&to!="c"?to:b?"#000000":"#FFFFFF",f=sbcRip(from),t=sbcRip(to);
  if(!f||!t)return null; //ErrorCheck
  if(h)return "rgba("+r((t[0]-f[0])*p+f[0])+","+r((t[1]-f[1])*p+f[1])+","+r((t[2]-f[2])*p+f[2])+(f[3]<0&&t[3]<0?")":","+(f[3]>-1&&t[3]>-1?r(((t[3]-f[3])*p+f[3])*10000)/10000:t[3]<0?f[3]:t[3])+")");
  else return "#"+(0x100000000+(f[3]>-1&&t[3]>-1?r(((t[3]-f[3])*p+f[3])*255):t[3]>-1?r(t[3]*255):f[3]>-1?r(f[3]*255):255)*0x1000000+r((t[0]-f[0])*p+f[0])*0x10000+r((t[1]-f[1])*p+f[1])*0x100+r((t[2]-f[2])*p+f[2])).toString(16).slice(f[3]>-1||t[3]>-1?1:3);
}
