Meteor.publish('user.private', function () {
  if (!this.userId) {
    return this.ready();
  }
  return Meteor.users.find({ _id: this.userId });
});

Meteor.publish('restaurant.private', function () {
  if (!this.userId) {
    return this.ready();
  }
  const u = Meteor.users.findOne({ _id: this.userId });
  return Restaurant.find({ _id: u.restaurant_id });
});

Meteor.publish('orders.private', function () {
  if (!this.userId) {
    return this.ready();
  }
  const u = Meteor.users.findOne({ _id: this.userId });
  const r = Restaurant.findOne({ _id: u.restaurant_id });
  if (r) {
    return Order.find({ restaurant_id: r._id });
  }
});


Meteor.publish('restaurant.public', function (rid) {
  return Restaurant.find({ _id: rid }, {
    fields : {
      'user_id': 0,
      'created': 0,
      'billing.status': 0,
      'billing.stripe_id': 0,
      'billing.subscription_id': 0,
      'billing.cancel_at': 0,
      'billing.last_payment': 0,
      'billing.locations': 0,
      'settings.status': 0,
      'orders': 0,
      'settings.payment.stripe.user_id': 0,
      'settings.payment.stripe.token_type': 0,
      'settings.payment.stripe.refresh_token': 0,
      'settings.payment.stripe.access_token': 0,
      'settings.payment.stripe.scope': 0,
    }
  });
});

Meteor.publish('order.public', function (oid) {
  return Order.find({ _id: oid});
});
