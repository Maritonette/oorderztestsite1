
SyncedCron.add({
  name: 'Automated Status Updates',
  schedule: function(parser) {
    // parser is a later.parse object
    return parser.text('every 30 seconds');
  },
  job: function() {
    Restaurant.find().forEach( r => {
      if (r.settings.status.auto.enabled) {
        let r_orders = Order.find({ restaurant_id : r._id });
        const times = r.settings.status.auto.time;
        const now = Date.now();
        r_orders.forEach((o, i) => {

          if (o.config.when !== 'Now') {
            if (o.status !== 'Cancelled' || o.status !== 'Complete') {

              // UNCONFIRMED GOES TO CONFIRM
              if (o.status == 'Un-Confirmed' && Number.isFinite(times.confirmed)) {
                console.log('Make UnConfirmed');
                if ((now - o.created) >= (times.confirmed * 60 * 1000)) {
                  Order.update({ _id: o._id }, { $set : { 'status': 'Confirmed' } })
                }
              }
              // CONFIRMED GOES TO READY
              else if (o.status == 'Confirmed' && Number.isFinite(times.ready)) {
                if ( (now - o.created) >= ((times.ready * 60 * 1000) + (times.confirmed * 60 * 1000) ) ) {
                  Order.update({ _id: o._id }, { $set : { 'status': 'Ready' } })
                }
              }
              // READY GOES TO COMPLETE IF PICKUP OR ON ROUTE IF DELIVERY
              else if (o.status == 'Ready' && o.config.logistic == 'Pickup' && Number.isFinite(times.complete)) {
                if ( (now - o.created) >= ( (times.complete * 60 * 1000) + (times.ready * 60 * 1000) + (times.confirmed * 60 * 1000) ) ) {
                  Order.update({ _id: o._id }, { $set : { 'status': 'Complete' } })
                }
              }
              else if (o.status == 'Ready' && o.config.logistic == 'Delivery' && Number.isFinite(times.on_route)) {
                if ( (now - o.created) >= ( (times.on_route * 60 * 1000) + (times.ready * 60 * 1000) + (times.confirmed * 60 * 1000) ) ) {
                  Order.update({ _id: o._id }, { $set : { 'status': 'On Route' } })
                }
              }
              // ON ROUTE GOES TO COMPLETE
              else if (o.status == 'On Route' && o.config.logistic == 'Delivery' && Number.isFinite(times.complete)) {
                if ( (now - o.created) >= ( (times.complete * 60 * 1000) + (times.on_route * 60 * 1000) + (times.ready * 60 * 1000) + (times.confirmed * 60 * 1000) ) ) {
                  Order.update({_id: o._id}, {$set: {'status': 'Complete'}})
                }
              }

            }
          }




        });
      }
    });
  }
});