import stripe from 'stripe';
const Stripe = stripe(Meteor.settings.private.stripe);

JsonRoutes.add("post", "/webhook/stripe/dhad39101dkjalsssd761hdcs5ps", function (req, res, next) {
  console.log('STRIPE WEBHOOK');
  console.log(req.body);
  let data = req.body;

  const getEvent = Meteor.wrapAsync(Stripe.events.retrieve, Stripe.events);

  try {
    // VERIFY EVENT
    const event = getEvent(data.id);
    if (data.type == 'invoice.payment_succeeded') {
      try {
        let invoice = data.data.object;
        const cust_id = invoice.customer;
        const sub_id = invoice.lines.data[0].id;
        const quantity = invoice.lines.data[0].quantity;
        const r = Restaurant.findOne({ 'billing.stripe_id': cust_id });
        // Confirm subscription is the same as users
        if (r.billing.subscription_id == sub_id) {
          Restaurant.update({ _id: r.id}, { $set: {
            'billing.locations': quantity,
            'billing.last_payment': invoice.date * 1000,
          }});
        }
        JsonRoutes.sendResult(res, {code:200});
      }
      catch(err) {
        console.log('Invoice.Payment.Succeeded Error');
        console.log(err);
      }
    }

    if (data.type == 'invoice.payment_failed') {
      try {
        let invoice = data.data.object;
        const cust_id = invoice.customer;
        const sub_id = invoice.lines.data[0].id;
        const r = Restaurant.findOne({ 'billing.stripe_id': cust_id });
        // Confirm subscription is the same as users
        if (r.billing.subscription_id == sub_id) {
          // DELETE CARD
          const deleteCard = Meteor.wrapAsync(Stripe.customers.deleteCard, Stripe.customers);
          let delCard = deleteCard(r.billing.stripe_id, r.billing.source);
          Restaurant.update({ _id: r.id}, { $set: { 'billing.source': null }});
        }
        JsonRoutes.sendResult(res, {code:200});
      }
      catch(err) {
        console.log('Invoice.Payment.Failed Error');
        console.log(err);
      }
    }

    if (data.type == 'customer.subscription.deleted') {
      try {
        let subscription = data.data.object;
        const cust_id = subscription.customer;
        const sub_id = subscription.id;
        const r = Restaurant.findOne({ 'billing.stripe_id': cust_id });
        if (r.billing.subscription_id == sub_id) {
          Restaurant.update({ _id: r.id}, { $set: {
            'billing.status': 3,
            'billing.subscription_id': null,
            'billing.cancel_at': subscription.current_period_end * 1000,
          }});
          JsonRoutes.sendResult(res, {code:200});
        }
      }
      catch(err) {
        console.log('customer.subscription.delete error');
        console.log(err);
      }
    }

  }
  catch(err) {
    console.log('Event failed validation');
    console.log(err);
  }

});
