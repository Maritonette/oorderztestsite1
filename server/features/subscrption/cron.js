
// 0 - Trial / 1 - Trial Expired / 2 - Subscribed / 3 - Payment Default / 4 - Cancelled

SyncedCron.add({
  name: 'Account Checker',
  schedule: function(parser) {
    // parser is a later.parse object
    return parser.text('every 1 minute');
  },
  job: function() {
    Restaurant.find().forEach( r => {

      const b = r.billing;
      const now = Date.now();

      if (b.status == 1) {
        const trial_period = (30 * 24 * 60 * 60 * 1000); // 30 days in millseconds
        if (now > (r.created + trial_period)) {
          // Trial expired, lock account trial expired
          Restaurant.update({ _id: r._id }, { $set : { 'billing.disable': true } })
        }
      }

      if (b.status == 3) {
        if (now > r.billing.cancel_at) {
          Restaurant.update({ _id: r._id }, { $set : { 'billing.disable': true } })
        }
      }

      /**
      if (b.status == 0) {
        const trial_period = (30 * 24 * 60 * 60 * 1000); // 30 days in millseconds
        if (now > (r.created + trial_period)) {
          // Trial expired, lock account trial expired
          Restaurant.update({ _id: r._id }, { $set : { 'billing.status': 1, 'billing.disable': true, 'billing.display_status': 'Trial Expired' } })
        }
      }

      if (b.status == 1) {

      }

      if (b.status == 2) {

      }

      if (b.status == 3) {
        // IF DEFAULTED FOR LONGER THAN 7 DAYS DISABLE
        const last_payment = r.billing.last_payment;
        if (now > (last_payment + (37 * 24 * 60 * 60 * 1000))) {
          Restaurant.update({ _id: r._id }, { $set : { 'billing.disable': true, 'billing.display_status': 'Payment Default - Disabled' } })
        }
      }

      if (b.status == 4) {
        // ONCE CANCEL PERIOD COMES TO HALT DISABLE
        if (now > r.billing.cancel_at) {
          Restaurant.update({ _id: r._id }, { $set : { 'billing.disable': true, 'billing.display_status': 'Cancelled - Disabled' } })
        }
      }
      */

    });
  }
});