// CREATE CUSTOMER WITH TOKEN SUBSCRIBE TO PLAY
// UPDATE CUSTOMER QUANTITY
// CANCEL SUBSCRIPTION

import stripe from 'stripe';
const Stripe = stripe(Meteor.settings.private.stripe);


Meteor.methods({
  'stripeUpdateCard'(token) {
    const u = Meteor.user();
    const r = Restaurant.findOne({ _id: u.restaurant_id });
    const updateCustomer = Meteor.wrapAsync(Stripe.customers.update, Stripe.customers);
    const deleteCard = Meteor.wrapAsync(Stripe.customers.deleteCard, Stripe.customers);
    try {
      const customer = updateCustomer(r.billing.stripe_id, { source: token.id });
      Restaurant.update({_id : r._id}, { $set: { 'billing.source': customer.sources.data[0].id }});
    }
    catch(err) {
      console.log('Stripe Add Card Error', err);
      if (err.type == 'StripeCardError') throw new Meteor.Error(err.message);
      else throw new Meteor.Error('Something went wrong and your details was not saved, please try again');
    }
  },
  'stripeCreateSubscription'(locs) {
    const u = Meteor.user();
    const r = Restaurant.findOne({ _id: u.restaurant_id });
    check(locs, Number);
    if (locs < 1) throw new Meteor.Error('Invalid number of locations');
    else if (r.settings.locations.length > locs) throw new Meteor.Error('You have more locations than you are trying to subscribe for');
    else if (!r.billing.source) throw new Meteor.Error('You do not have an active payment method, please add one first');
    else {

      const createSubscription = Meteor.wrapAsync(Stripe.subscriptions.create, Stripe.subscriptions);
      try {

        let subscriptionDetails = {
          customer: r.billing.stripe_id,
          plan: "single-location",
          quantity: locs
        };
        if ((r.created + (30 * 24 * 60 * 60 * 1000)) > Date.now()) {
          subscriptionDetails.trial_end = (parseInt(r.created / 1000)) + (30 * 24 * 60 * 60); // Takes seconds not millseconds
        }
        else if (r.billing.cancel_at && r.billing.cancel_at > Date.now()) {
          subscriptionDetails.trial_end = (parseInt(r.billing.cancel_at / 1000));
        }

        const subscription = createSubscription(subscriptionDetails);
        Restaurant.update({_id : r._id}, { $set: {
          'billing.status': 2,
          'billing.disable': false,
          'billing.subscription_id': subscription.id,
          'billing.locations': subscription.quantity,
          'billing.last_payment': subscription.created * 1000,
          'billing.cancel_at': null,
        }});
      }
      catch(err) {
        console.log('stripeSubscribeRestaurant Error', err);
        if (err.type == 'StripeCardError') {
          Restaurant.update({_id : r._id}, { $set: { 'billing.source': false }}); // Remove active payment method
          throw new Meteor.Error(err.message);
        }
        throw new Meteor.error("Something went wrong and you couldn't be subscribed, please try again or contact us.")
      }

    }
  },
  'stripeUpdateSubscription'(locs) {
    const u = Meteor.user();
    const r = Restaurant.findOne({ _id: u.restaurant_id });
    check(locs, Number);
    if (locs < 1) throw new Meteor.Error('Invalid number of locations');
    else if (r.settings.locations.length > locs) throw new Meteor.Error('You have more locations than you are trying to subscribe for');
    else if (!r.billing.source) throw new Meteor.Error('You do not have an active payment method, please add one first');
    else if (r.billing.locations == locs) throw new Meteor.Error('Already subscribed to the chosen amount of locations');
    else {
      const updateSubscription = Meteor.wrapAsync(Stripe.subscriptions.update, Stripe.subscriptions);
      try {
        const subscription = updateSubscription(r.billing.subscription_id, { quantity: locs });
        Restaurant.update({ _id: r._id }, { $set: {
          'billing.locations': subscription.quantity
        }});
      }
      catch(err) {
        console.log('stripeUpdateRestaurant Error', err);
        throw new Meteor.error("Something went wrong and your subscription couldn't be updated, please try again or contact us.")
      }
    }
  },
  'stripeCancelSubscription'() {
    const u = Meteor.user();
    const r = Restaurant.findOne({ _id: u.restaurant_id });
    const delSubscription = Meteor.wrapAsync(Stripe.subscriptions.del, Stripe.subscriptions);
    try {
      const subscription = delSubscription(r.billing.subscription_id, { at_period_end: true });
      Restaurant.update({ _id: r._id }, { $set: {
        'billing.status': 3,
        'billing.subscription_id': null,
        'billing.cancel_at': subscription.current_period_end * 1000,
      }});
    }
    catch(err) {
      console.log('stripeCancelSubscription Error', err);
      throw new Meteor.error("Something went wrong and your subscription couldn't be delete. Please try again or contact us.")
    }
  },
  'stripeGetInvoices'() {
    const u = Meteor.user();
    const r = Restaurant.findOne({ _id: u.restaurant_id });
    const getInvoices = Meteor.wrapAsync(Stripe.invoices.list, Stripe.invoices);
    try {
      return getInvoices({ customer: r.billing.stripe_id });
    }
    catch(err) {
      throw new Meteor.Error(err.message)
    }
  },
});