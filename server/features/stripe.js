import stripe from 'stripe';
const Stripe = stripe(Meteor.settings.private.stripe);

const getStripeAccountPromise = (stripe_user_id) => {
  return new Promise((resolve, reject) => {
    Stripe.accounts.retrieve(stripe_user_id, (err, result) => {
      if (err) reject(err);
      else resolve(result);
    });
  });
};

Meteor.methods({
  'connectStripe'(code){
    check(code, String);
    const c = code;
    const client_id = Meteor.settings.private.client_id;
    const client_secret = Meteor.settings.private.stripe;
    HTTP.call("POST", "https://connect.stripe.com/oauth/token", {
        data: {
          code: c,
          client_secret: client_secret,
          grant_type: "authorization_code"
        }
      },
      function (error, result) {
        if (error) console.log("Stripe Connect Error: " + error);
        else {
          const u = Meteor.user();
          Restaurant.update({ _id: u.restaurant_id }, { $set : {
            "settings.payment.credit_card.enabled": true,
            "settings.payment.stripe.connected": true,
            "settings.payment.stripe.token_type": result.data.token_type,
            "settings.payment.stripe.publishable_key": result.data.stripe_publishable_key,
            "settings.payment.stripe.user_id": result.data.stripe_user_id,
            "settings.payment.stripe.refresh_token": result.data.refresh_token,
            "settings.payment.stripe.access_token": result.data.access_token,
            "settings.payment.stripe.scope": result.data.scope,
            "settings.payment.stripe.livemode": result.data.livemode,
            "settings.payment.stripe.charges_enabled": false,
          }}, () => {
            const validateAccount = getStripeAccountPromise(result.data.stripe_user_id);
            validateAccount.then((res) => {
              Restaurant.update({ _id: u.restaurant_id }, { $set : {
                "settings.payment.stripe.charges_enabled": res.charges_enabled,
                "settings.payment.stripe.default_currency": res.default_currency,
              }});
            }).catch((err) => { console.log('Validate account error: ' + err) });
          });
        }
      });
  },
  'disconnectStripe'() {
    const client_id = Meteor.settings.private.client_id;
    const client_secret = Meteor.settings.private.stripe;
    const user = Meteor.user();
    const r = Restaurant.findOne({ _id: user.restaurant_id });
    if (r) {
      const stripeUser = r.settings.payment.stripe.user_id;
      HTTP.call("POST", "https://connect.stripe.com/oauth/deauthorize", {
          data: {
            client_secret: client_secret,
            client_id: client_id,
            stripe_user_id: stripeUser
          }
        },
        function (error, result) {
          if (error) console.log("Stripe Disconnect Error: " + error);
          else {
            Restaurant.update({ _id: r._id }, { $set : {
              "settings.payment.credit_card.enabled": false,
              "settings.payment.stripe.connected": false,
              "settings.payment.stripe.token_type": null,
              "settings.payment.stripe.publishable_key": null,
              "settings.payment.stripe.user_id": null,
              "settings.payment.stripe.refresh_token": null,
              "settings.payment.stripe.access_token": null,
              "settings.payment.stripe.scope": null,
              "settings.payment.stripe.livemode": null,
              "settings.payment.stripe.charges_enabled": null,
              "settings.payment.stripe.default_currency": null
            }});
          }
        });
    }
  },
  'validateStripe'() {
    const user = Meteor.user();
    const r = Restaurant.findOne({ _id: user.restaurant_id });
    if (r) {
      const stripe_user_id = r.settings.payment.stripe.user_id;
      const validateAccount = getStripeAccountPromise(stripe_user_id);
      validateAccount.then((res) => {
        Restaurant.update({ _id: user.restaurant_id }, { $set : {
          "settings.payment.stripe.charges_enabled": res.charges_enabled,
          "settings.payment.stripe.default_currency": res.default_currency,
        }});
      }).catch((err) => { console.log('Validate account error: ' + err) });
    }
  },
});