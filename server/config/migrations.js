import { Migrations } from 'meteor/percolate:migrations';

Migrations.config({
  // Log job run details to console
  log: true,

  // Use a custom logger function (defaults to Meteor's logging package)
  logger: null,

  // Enable/disable logging "Not migrating, already at version {number}"
  logIfLatest: true,
});

/**
Migrations.add({
  version: 4,
  name: 'Updating order status data',
  up(){
    Order.find({ 'status.confirmed': {$exists: true}}).forEach( o => {
      Order.update({ _id: o._id }, { $set: { 'status': "Un-Confirmed" } }, (err) => {
        if (err) console.log(err);
      });
    });
  }
});

Migrations.add({
  version: 3,
  name: 'Adding location coordinate fields',
  up(){
    Restaurant.find().forEach( r => {
      r.settings.locations.forEach((l, k) => {
        if (!l.map_data) {
          Restaurant.update({ _id: r._id, "settings.locations": { $elemMatch: { _id: l._id } } },
            { $set: { 'settings.locations.$.map_data': {
              geometry: { lat: null, lng: null }
            }}}, (error) => {
              if (error) console.log(error);
          });
        }
      });
    });
  },
});

Migrations.add({
  version: 2,
  name: 'Adding store design features',
  up(){
    Restaurant.find({ 'settings.design': {$exists: false}}).forEach( r => {
      let settings = r.settings;
      settings.design = {
        colors: {
          primary : "rgba(248,153,75,1)",
          primary_text : "rgba(255,255,255,1)",
          main_text : "rgba(19,19,19,1)",
          menu_text : "rgba(167,177,194,1)",
          background : "rgba(19,19,19,1)",
          box : "rgba(243,243,244,1)",
          product_box : "rgba(255,255,255,1)",
          menu : "rgba(25,25,25,1)"
        },
        images: {
          logo: { url: null, rel: null },
          background: { url: null, rel: null, stretch: false }
        }
      };
      Restaurant.update({ _id: r._id }, { $set: { 'settings': settings } }, (err) => {
        if (err) console.log(err);
      });
    });
  },
  down(){
    Restaurant.update({}, {$unset: { 'settings.design': true }});
  }
});

Migrations.add({
  version: 1,
  name: 'Add terms & conditions + contact email',
  up(){

  },
  down(){

  }
});
*/