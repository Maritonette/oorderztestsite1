import { Migrations } from 'meteor/percolate:migrations';

Meteor.startup(() => {
  // EMAIL SERVER LINK
  process.env.MAIL_URL = "smtp://postmaster%40oorderz.com:606157e61eaf1131631caebf59126b79@smtp.mailgun.org:587";

  Migrations.migrateTo('latest');
});