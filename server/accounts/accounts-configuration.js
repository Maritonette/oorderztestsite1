/**
 * Created by theNiglet on 10/04/2016.
 */

import { Accounts } from 'meteor/accounts-base';

Accounts.onCreateUser(function (options, user) {
  user.account = {
    restaurant_id: null,
  };
  // We still want the default hook's 'profile' behavior.
  if (options.profile){
    user.profile = options.profile;
  }
  return user;
});

