Accounts.emailTemplates.siteName = "OORDERZ";
Accounts.emailTemplates.from     = "OORDERZ <no-reply@oorderz.com>";

Accounts.emailTemplates.verifyEmail = {
  subject() {
    return "Oorderz - Verify Your Email Address";
  },
  text( user, url ) {
    let emailAddress   = user.emails[0].address,
      urlWithoutHash = url.replace( '#/', '' ),
      supportEmail   = "support@oorderz.com",
      emailBody      = `Welcome to Oorderz.\n\nPlease complete your registration by visiting the following link to verify your email\n\n${urlWithoutHash}\n\n If you did not request this verification, please ignore this email. If you feel something is wrong, please contact our support team: ${supportEmail}`;

    return emailBody;
  }
};

Accounts.emailTemplates.resetPassword = {
  subject() {
    return "Oorderz - Password Reset";
  },
  text( user, url ) {
    let emailAddress   = user.emails[0].address,
      urlWithoutHash = url.replace( '#/', '' ),
      supportEmail   = "support@oorderz.com",
      emailBody      = `Please follow the link below to reset your password\n\n${urlWithoutHash}\n\n If you did not request to reset your password, please ignore this email. If you feel something is wrong, please contact our support team: ${supportEmail}`;

    return emailBody;
  }
};
