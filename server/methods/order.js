import stripe from 'stripe';
const Stripe = stripe(Meteor.settings.private.stripe);

Meteor.methods({
  // MAIN RESTAURANT METHODS
  'createOrder'(o) {
    const r = Restaurant.findOne({_id: o.restaurant_id });
    if (!r) {
      throw new Meteor.Error('This restaurant does not exist');
    }
    else {
      let min_order = r.settings.logistic.delivery.conditions.min_order;
      if (min_order && min_order > o.total) {
        throw new Meteor.Error('Order total is less than the minimum delivery');
      }
      else {
        const createOrder = Meteor.wrapAsync(Order.insert, Order);
        try {
          return createOrder(o);
        }
        catch (err) {
          console.log(err)
        }
      }
    }
  },
  'createOrderStripe'(o, token) {
    const r = Restaurant.findOne({_id: o.restaurant_id });
    if (!r) {
      throw new Meteor.Error('This restaurant does not exist');
    }
    else {
      let min_order = r.settings.logistic.delivery.conditions.min_order;
      if (min_order && min_order > o.total) {
        throw new Meteor.Error('Order total is less than the minimum delivery');
      }
      else {
        const chargeCard = Meteor.wrapAsync(Stripe.charges.create, Stripe.charges);
        let cardCharged = null;
        try {
          cardCharged = chargeCard({
            amount: o.total * 100,
            currency: r.settings.payment.stripe.default_currency,
            source: token.id,
            "metadata": {
              customer: o.customer.name,
              email: o.customer.email,
              phone: o.customer.phone
            }
          }, {
            stripe_account: r.settings.payment.stripe.user_id
          })
        }
        catch (err) {
          console.log(err);
          if (err && err.type === 'StripeCardError') {
            return 'Card has been declined'
          }
        }
        if (cardCharged) {
          o.config.payment.captured = true;
          const createOrder = Meteor.wrapAsync(Order.insert, Order);
          try {
            return createOrder(o);
          }
          catch (err) {
            console.log(err);
            return "Something went wrong, you were charged but your order couldn't go through";
          }
        }
      }
    }
  },
  'updateOrderCancelled'(id) {
    const o = Order.findOne({_id : id});
    const r = Restaurant.findOne({ _id: o.restaurant_id});
    const u = Meteor.user();
    if (u.restaurant_id === r._id) {
      Order.update({ _id: id }, { $set: { 'status': "Cancelled" } });
    }
  },
  'updateOrderUnconfirmed'(id) {
    const o = Order.findOne({_id : id});
    const r = Restaurant.findOne({ _id: o.restaurant_id});
    const u = Meteor.user();
    if (u.restaurant_id === r._id) {
      Order.update({ _id: id }, { $set: { 'status': "Un-Confirmed" } });
    }
  },
  'updateOrderConfirmed'(id) {
    const o = Order.findOne({_id : id});
    const r = Restaurant.findOne({ _id: o.restaurant_id});
      const u = Meteor.user();
      if (u.restaurant_id === r._id) {
        Order.update({ _id: id }, { $set: { 'status': "Confirmed" } });
      }
  },
  'updateOrderReady'(id) {
    const o = Order.findOne({_id : id});
    const r = Restaurant.findOne({ _id: o.restaurant_id});
      const u = Meteor.user();
      if (u.restaurant_id === r._id) {
        Order.update({ _id: id }, { $set: { 'status': "Ready" } });
      }
  },
  'updateOrderOnRoute'(id) {
      const o = Order.findOne({_id : id});
      const r = Restaurant.findOne({ _id: o.restaurant_id});
      const u = Meteor.user();
      if (u.restaurant_id === r._id) {
        Order.update({ _id: id }, { $set: { 'status': "On Route" } });
      }
  },
  'updateOrderComplete'(id) {
      const o = Order.findOne({_id : id});
      const r = Restaurant.findOne({ _id: o.restaurant_id});
      const u = Meteor.user();
      if (u.restaurant_id === r._id) {
        Order.update({ _id: id }, { $set: { 'status': "Complete" } });
      }
  }
});
