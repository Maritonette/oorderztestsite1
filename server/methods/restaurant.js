import { currencyList } from '../../lib/data_sets.js';
import stripe from 'stripe';
const Stripe = stripe(Meteor.settings.private.stripe);

Meteor.methods({
  // MAIN RESTAURANT METHODS
  'createRestaurant'(r) {
    const u = Meteor.user();
    const rest = Restaurant.findOne({ user_id: Meteor.userId() });
    if (rest) {
      throw new Meteor.Error('There is already a restaurant created for this account, please create a new login');
    }
    else {
      const restaurant = r;

      // FIND CURRENCY INDEX
      let cIndex = null;
      const cL = currencyList;
      cL.forEach((c, i) => { if (c._id == r.settings.region.currency) cIndex = i; });
      let symbol = null;
      if (cL[cIndex].symbol == '' || cL[cIndex].symbol == '?' || cL[cIndex].symbol == '??' || cL[cIndex].symbol == '???') symbol = cL[cIndex]._id;
      else symbol = cL[cIndex].symbol;
      restaurant.settings.region.currency_symbol = symbol;

      try {
        const createStripeCustomer = Meteor.wrapAsync(Stripe.customers.create, Stripe.customers);
        const createRestaurant = Meteor.wrapAsync(Restaurant.insert, Restaurant);

        // Create Restaurant
        const createdRestaurant = createRestaurant(restaurant);
        // Set user restaurant id
        Meteor.users.update({ _id: u._id }, { $set: { "restaurant_id": createdRestaurant } });
        // Create stripe customer with these details
        const stripeCustomer = createStripeCustomer({
          email: u.emails[0].address,
          description: `Restaurant ${createdRestaurant}`,
          metadata: {
            user: u._id,
            restaurant: createdRestaurant
          }
        });
        // UPDATE RESTAURANT
        Restaurant.update({ _id: createdRestaurant }, { $set: { "billing.stripe_id": stripeCustomer.id } });

        return true

      }
      catch(err) {
        console.log(err);
        throw new Meteor.Error(err.message)
      }


    }
  },
  'updateRestaurantBasic'(name, info) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: { 'name': name, 'settings.info': info } });
  },
  'updateRestaurantLocations'(locations) {
    const u = Meteor.user();
    const r = Restaurant.findOne({ _id: u.restaurant_id });
    if (locations.length > r.billing.locations) {
      throw new Meteor.Error(`You have only subscribed for ${r.billing.locations} locations. See the billing page to get more locations`)
    }
    else {
      Restaurant.update({ _id: u.restaurant_id }, { $set: { 'settings.locations': locations} });
    }
  },
  'updateRestaurantRegion'(region) {
    const u = Meteor.user();

    // Set region currency symbol
    let cIndex = null;
    const cL = currencyList;
    cL.forEach((c, i) => {
      if (c._id == region.currency) cIndex = i;
    });
    let symbol = null;
    if (cL[cIndex].symbol == '' || cL[cIndex].symbol == '?' || cL[cIndex].symbol == '??' || cL[cIndex].symbol == '???') {
      symbol = cL[cIndex]._id;
    }
    else symbol = cL[cIndex].symbol;

    region.currency_symbol = symbol;
    Restaurant.update({ _id: u.restaurant_id }, { $set: { 'settings.region': region} });
  },
  'updateRestaurantLogistic'(logistic) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: { 'settings.logistic': logistic} });
  },
  'updateRestaurantPayment'(payment) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: {
      'settings.payment.cash': payment.cash,
      'settings.payment.efptos': payment.efptos,
      'settings.payment.credit_card': payment.credit_card
    } });
  },
  'updateRestaurantStatus'(status) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: { 'settings.status': status } });
  },
  'updateRestaurantDesign'(design) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: { 'settings.design': design } });
  },
  'deleteLogoImage'() {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: {
      'settings.design.images.logo': { url: null, rel: null },
    }
    });
  },
  'deleteBackgroundImage'() {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $set: {
      'settings.design.images.background.url': null,
      'settings.design.images.background.rel': null
    }
    });
  },
  'restaurantOpenLocation'(id) {
    check(id, String);
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, "settings.locations": { $elemMatch: { _id: id } } },
      { $set: { 'settings.locations.$.open': true}}, (error) => {
        if (error) console.log(error);
      });
  },
  'restaurantCloseLocation'(id) {
    check(id, String);
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, "settings.locations": { $elemMatch: { _id: id } } },
      { $set: { 'settings.locations.$.open': false}}, (error) => {
        if (error) console.log(error);
      });
  },
});