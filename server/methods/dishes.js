Meteor.methods({
  'createDish'(d) {
    d._id = new Mongo.ObjectID()._str;
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $push: { dishes: d } }, (error) => {
      if (error) console.log(error);
    });
  },
  'deleteDish'(id) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $pull: { dishes: { _id: id } } }, (error) => {
      if (error) console.log(error);
    });
  },
  'editDish'(dish) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, dishes: { $elemMatch: { _id: dish._id } } },
      { $set: { 'dishes.$': dish },
      }, (error) => {
        if (error) console.log(error);
      });
  },
  'deleteDishImage'(id) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, dishes: { $elemMatch: { _id: id } } },
      { $set: {
        'dishes.$.image.rel': null,
        'dishes.$.image.url': null
      },
      }, (error) => {
        if (error) console.log(error);
      });
  },
  // CATEGORIES
  'createCategory'(category) {
    const u = Meteor.user();
    category._id = new Mongo.ObjectID()._str;
    Restaurant.update({ _id: u.restaurant_id }, { $push: { categories: category } }, (error) => {
      if (error) console.log(error);
    });
  },
  'deleteCategory'(id) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $pull: { categories: { _id: id } } }, (error) => {
      if (error) console.log(error);
    });
  },
  'editCategory'(category, id) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, categories: { $elemMatch: { _id: id } } },
      { $set: { 'categories.$': category
      }}, (error) => {
        if (error) console.log(error);
      });
  },
  // OPTION SETS
  'createOptionSet'(option_set) {
    const u = Meteor.user();
    option_set._id = new Mongo.ObjectID()._str;
    Restaurant.update({ _id: u.restaurant_id }, { $push: { option_sets: option_set} }, (error) => {
      if (error) console.log(error);
    });
  },
  'deleteOptionSet'(id) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id }, { $pull: { option_sets: { _id: id } } }, (error) => {
      if (error) console.log(error);
    });
  },
  'editOptionSet'(option_set) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, option_sets: { $elemMatch: { _id: option_set._id } } },
      { $set: { 'option_sets.$': option_set
      }}, (error) => {
        if (error) console.log(error);
      });
  },
  'createOption'(option, setId) {
    const u = Meteor.user();
    option._id = new Mongo.ObjectID()._str;
    Restaurant.update({ _id: u.restaurant_id, option_sets: { $elemMatch: { _id: setId } } },
      { $push: { 'option_sets.$.options': option }}, (error) => {
        if (error) console.log(error);
      });
  },
  'deleteOption'(id, setId) {
    const u = Meteor.user();
    Restaurant.update({ _id: u.restaurant_id, option_sets: { $elemMatch: { _id: setId } } },
      { $pull: { 'option_sets.$.options': { _id: id } }}, (error) => {
        if (error) console.log(error);
      });
  },
});