Meteor.methods({
  sendStoreContactEmail(message, id) {
    const r = Restaurant.findOne({ _id: id });
    let text = `Name: ${message.name}\nContact: ${message.contact}\n\n${message.message}`;
    Email.send({
      to: r.settings.info.store_email,
      from: "Oorderz Store Form <no-reply@oorderz.com>",
      subject: `${message.name}`,
      text: text,
    });
  }
});
